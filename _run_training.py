""" This script runs every training.

Only personachat available at the beginning ...
"""
import os
import sys
import torch
import logging
import multiprocessing
from argparse import ArgumentParser
from configparser import ConfigParser
from train_scripts import train_personachat as tr_ps
from train_scripts import train_moviecorpus as tr_mc
from train_scripts import train_opendialkg as tr_od
from train_scripts import train_komodis_nli as tr_kom_nli


import transformers
print(transformers.__version__)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--dataset", type=str)
    parser.add_argument("--dataset_path", type=str, default="",
                        help="Path or url of the dataset. If empty download from S3.")
    parser.add_argument("--dataset_cache", type=str, default='./dataset_cache', help="Path or url of the dataset cache")
    parser.add_argument("--model_checkpoint", type=str, default="openai-gpt",
                        help="Path, url or short name of the model")
    parser.add_argument("--experiment", type=str, default=None, help="A unique name for the current training.")
    parser.add_argument("--num_candidates", type=int, default=4, help="Number of candidates for training")
    parser.add_argument("--max_history", type=int, default=2, help="Number of previous exchanges to keep in history")
    parser.add_argument("--train_batch_size", type=int, default=4, help="Batch size for training")
    parser.add_argument("--valid_batch_size", type=int, default=2, help="Batch size for validation")
    parser.add_argument("--gradient_accumulation_steps", type=int, default=8,
                        help="Accumulate gradients on several steps")
    parser.add_argument("--lr", type=float, default=6.25e-5, help="Learning rate")
    parser.add_argument("--lm_coef", type=float, default=1.0, help="LM loss coefficient")
    parser.add_argument("--mc_coef", type=float, default=1.0, help="Multiple-choice loss coefficient")
    parser.add_argument("--max_norm", type=float, default=1.0, help="Clipping gradient norm")
    parser.add_argument("--n_epochs", type=int, default=3, help="Number of training epochs")
    parser.add_argument("--stop_after", type=int, default=-1, help="Stops after n epochs. Use this for early stopping.")
    parser.add_argument("--personality_permutations", type=int, default=1,
                        help="Number of permutations of personality sentences")
    parser.add_argument("--eval_before_start", action='store_true',
                        help="If true start with a first evaluation before training")
    parser.add_argument("--device", type=str, default="cuda:1" if torch.cuda.is_available() else "cpu",
                        help="Device (cuda or cpu)")
    parser.add_argument("--no_data_parallel", action="store_true", help="If True, the distributed training must used!")
    parser.add_argument("--fp16", type=str, default="",
                        help="Set to O0, O1, O2 or O3 for fp16 training (see apex documentation)")
    parser.add_argument("--local_rank", type=int, default=-1,
                        help="Local rank for distributed training (-1: not distributed)")
    parser.add_argument("--num_gpus", type=int, default=4, help="Number of GPUs. Only used if device==gpu.")
    parser.add_argument("--max_input_length", type=int, default=256, help="The maximum length of sequences for "
                                                                          "training. All samples are padded to that "
                                                                          "length.")
    parser.add_argument("--tensorboard", action="store_true", help="If True, tensorboard will be used to log training.")
    # --- general parameter ---
    parser.add_argument("--data_per_epoch_strategy", type=str, default="", help="A strategy that specifies how the "
                                                                                "training data should be changed "
                                                                                "during epochs. See base_trainer.py "
                                                                                "for more information.")
    # --- PERSONACHAT parameter ---
    parser.add_argument("--train_with_revised", action="store_true", help="If set, the personachat dataset is trained "
                                                                          "on the revised personas instead of the "
                                                                          "originals.")
    parser.add_argument("--full_context_separation", action="store_true", help="If True, then the dialogue history "
                                                                               "always starts after a set amount of "
                                                                               "tokens. "
                                                                               "Requires max_context_length > 0")
    parser.add_argument("--remove_bos_token", action="store_true", help="If kg encoding, remove the first token.")
    # --- KOMODIS parameter ---
    parser.add_argument("--attitude_sentences", action="store_true", help="If set, the attitudes are generated as "
                                                                          "real sentences instead of single tokens.")
    parser.add_argument("--remove_delex", action="store_true", help="If True, the delexication on entities is not"
                                                                    "done. Instead the real values are fed in.")
    parser.add_argument("--context_triples", action="store_true", help="If True, Context is encoded as real triples "
                                                                       "to match the kg_encoding style.")
    parser.add_argument("--kg_encoding", action="store_true", help="If True, the new knowledge graph encoding "
                                                                   "strategy is implemented.")
    parser.add_argument("--kg_encoding_type", type=str, default="komodis", help="One of: komodis, edge_node, smart")
    parser.add_argument("--use_graphs_as_triples", action="store_true", help="If True, the context is sequenced based "
                                                                             "on the data from sub_graphs.")
    parser.add_argument("--difficult_wrong_candidates", action="store_true", help="If True, the chosen wrong utterances"
                                                                                  " are more difficult to learn.")
    parser.add_argument("--same_movie_candidates", action="store_true", help="If True, wrong candidates are only "
                                                                             "processed from same movie titles.")
    parser.add_argument("--max_context_length", type=int, default=-1,
                        help="The maximum length of the context. Information is removed until this value fits.")
    # TODO: rename to context_depth; since we added that variable for the facts as well.
    parser.add_argument("--graph_depth", type=int, default=0, help="The depth of the used knowledge graph.")
    # --- KOMODIS NLI parameter ---
    parser.add_argument("--cross_validation", type=str, default="deactivated",
                        help="Can be either automatic, manual or manual_final.")
    parser.add_argument("--cv_k_fold", type=int, default=5, help="k-value for k-fold cross validation.")
    parser.add_argument("--cv_current", type=int, default=0, help="For manual cross validation. Determines which "
                                                                  "validation split should be used for training.")
    parser.add_argument("--context_paraphrased", action="store_true", help="If True, facts are encoded as sentences.")
    parser.add_argument("--weight_decay", type=float, default=0.1, help="Weight decay parameter.")
    parser.add_argument("--warmup_rel", type=float, default=0.06, help="Percentage of warmup-steps.")
    # -------------------------
    parser.add_argument("--debug", action="store_true", help="If true only a slice of the data is processed and "
                                                             "some samples are displayed on console.")
    args = parser.parse_args()

    logger = logging.getLogger(__file__)

    config_parser = ConfigParser()
    curr_dir = os.path.dirname(os.path.realpath(__file__))
    config_parser.read(os.path.join(curr_dir, "config.ini"))
    if args.dataset_path == "":
        path_to_data = config_parser.get("datasets", "path_{}".format(args.dataset))
    else:
        path_to_data = args.dataset_path
    path_to_tensorboard = config_parser.get("tensorboard", "path_to_logs")
    args.path_to_runs = config_parser.get("paths", "runs")
    args.path_to_tests = config_parser.get("paths", "tests")

    if args.device != "cpu":
        multiprocessing.set_start_method("forkserver")

    if args.cross_validation == "manual":
        print("WARNING: Cross validation is set to manual. No model weights are saved after training!")
        save_checkpoints = False
    else:
        save_checkpoints = True

    if args.dataset == "personachat":
        # TODO: Change this like KOMODIS!
        trainer = tr_ps.PersonachatTrainer(path_to_pretrained_model="data/pretrained_models/gpt2/",
                                           path_to_vocab_file="data/tokenizers/gpt2-vocab.json",
                                           path_to_merges_file="data/tokenizers/gpt2-merges.txt",
                                           path_to_txt_files="data/datasets/personachat",
                                           path_to_encoded_data="data/datasets/personachat/"
                                                                "personachat_gpt_openai_tokenized.pkl",
                                           hparams=args)
    elif args.dataset == "komodis":
        trainer = tr_mc.MoviecorpusTrainer(path_to_pretrained_model=args.model_checkpoint,
                                           path_to_data=path_to_data,
                                           hps=args,
                                           logger=logger)
    elif args.dataset == "opendialkg":
        trainer = tr_od.OpenDialKGTrainer(path_to_pretrained_model=args.model_checkpoint,
                                          path_to_data=path_to_data,
                                          hps=args,
                                          logger=logger)

    elif args.dataset == "komodis_nli":
        trainer = tr_kom_nli.KOMODISNLITrainer(path_to_pretrained_model=args.model_checkpoint,
                                               path_to_data=path_to_data,
                                               hps=args,
                                               path_to_tensorboard=path_to_tensorboard,
                                               logger=logger)
    else:
        print("{} not implemented.".format(args.dataset))
        sys.exit()

    trainer.train(save_checkpoints=save_checkpoints)
