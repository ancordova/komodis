# # Copyright (c) 2019-present, HuggingFace Inc.
# All rights reserved.
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree.
import logging
import random
from argparse import ArgumentParser
from itertools import chain
from pprint import pformat
import warnings

import torch
import torch.nn.functional as F

from transformers import OpenAIGPTLMHeadModel, OpenAIGPTTokenizer, GPT2LMHeadModel, GPT2Tokenizer
from train_movies import SPECIAL_TOKENS, build_input_from_segments, add_special_tokens_
from utils import get_dataset, download_pretrained_model, load_dataset_utils
from moviecorpus import *
import os


def top_filtering(logits, top_k=0., top_p=0.9, threshold=-float('Inf'), filter_value=-float('Inf')):
    """ Filter a distribution of logits using top-k, top-p (nucleus) and/or threshold filtering
        Args:
            logits: logits distribution shape (vocabulary size)
            top_k: <=0: no filtering, >0: keep only top k tokens with highest probability.
            top_p: <=0.0: no filtering, >0.0: keep only a subset S of candidates, where S is the smallest subset
                whose total probability mass is greater than or equal to the threshold top_p.
                In practice, we select the highest probability tokens whose cumulative probability mass exceeds
                the threshold top_p.
            threshold: a minimal threshold to keep logits
    """
    assert logits.dim() == 1  # Only work for batch size 1 for now - could update but it would obfuscate a bit the code
    top_k = min(top_k, logits.size(-1))
    if top_k > 0:
        # Remove all tokens with a probability less than the last token in the top-k tokens
        indices_to_remove = logits < torch.topk(logits, top_k)[0][..., -1, None]
        logits[indices_to_remove] = filter_value

    if top_p > 0.0:
        # Compute cumulative probabilities of sorted tokens
        sorted_logits, sorted_indices = torch.sort(logits, descending=True)
        cumulative_probabilities = torch.cumsum(F.softmax(sorted_logits, dim=-1), dim=-1)

        # Remove tokens with cumulative probability above the threshold
        sorted_indices_to_remove = cumulative_probabilities > top_p
        # Shift the indices to the right to keep also the first token above the threshold
        sorted_indices_to_remove[..., 1:] = sorted_indices_to_remove[..., :-1].clone()
        sorted_indices_to_remove[..., 0] = 0

        # Back to unsorted indices and set them to -infinity
        indices_to_remove = sorted_indices[sorted_indices_to_remove]
        logits[indices_to_remove] = filter_value

    indices_to_remove = logits < threshold
    logits[indices_to_remove] = filter_value

    return logits


# ByMe
def sample_sequence(obj, facts, attitudes, history, tokenizer, model, args, current_output=None):
    special_tokens_ids = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS)
    if current_output is None:
        current_output = []

    for i in range(args.max_length):
        instance = obj.convert_clear_txt_to_sequences(facts, attitudes, history, reply=current_output)
        input_ids = torch.tensor(instance["input_ids"], device=args.device).unsqueeze(0)
        token_type_ids = torch.tensor(instance["token_type_ids"], device=args.device).unsqueeze(0)

        logits = model(input_ids, token_type_ids=token_type_ids)
        logits = logits[0]

        # Keep only the last token predictions of the first batch item (batch size 1),
        # apply a temperature coefficient and filter
        logits = logits[0, -1, :] / args.temperature
        logits = top_filtering(logits, top_k=args.top_k, top_p=args.top_p) # [50296]
        probs = F.softmax(logits, dim=-1)

        prev = torch.topk(probs, 1)[1] if args.no_sample else torch.multinomial(probs, 1)
        if i < args.min_length and prev.item() in special_tokens_ids:
            while prev.item() in special_tokens_ids:
                if probs.max().item() == 1:
                    warnings.warn("Warning: model generating special token with probability 1.")
                    break  # avoid infinitely looping over special token
                prev = torch.multinomial(probs, num_samples=1)


        if prev.item() in special_tokens_ids:
            break
        current_output.append(prev.item())

    return current_output


def run(display=True):
    parser = ArgumentParser()
    parser.add_argument("--dataset_path", type=str, default="",
                        help="Path or url of the dataset. If empty download from S3.")
    parser.add_argument("--dataset_cache", type=str, default='./dataset_cache', help="Path or url of the dataset cache")
    parser.add_argument("--model", type=str, default="gpt2", help="Model type (openai-gpt or gpt2)",
                        choices=['openai-gpt', 'gpt2'])  # anything besides gpt2 will load gpt2
    parser.add_argument("--model_checkpoint", type=str, default="gpt2", help="Path, url or short name of the model")
    parser.add_argument("--max_history", type=int, default=4, help="Number of previous utterances to keep in history")
    parser.add_argument("--device", type=str, default="cuda" if torch.cuda.is_available() else "cpu",
                        help="Device (cuda or cpu)")
    parser.add_argument("--conversations", type=int, default=100, help="Number of artificial conversations")
    parser.add_argument("--no_sample", action='store_true', help="Set to use greedy decoding instead of sampling")
    parser.add_argument("--max_length", type=int, default=64, help="Maximum length of the output utterances")
    parser.add_argument("--min_length", type=int, default=1, help="Minimum length of the output utterances")
    parser.add_argument("--seed", type=int, default=0, help="Seed")
    parser.add_argument("--temperature", type=float, default=0.7, help="Sampling softmax temperature")
    parser.add_argument("--top_k", type=int, default=0, help="Filter top-k tokens before sampling (<=0: no filtering)")
    parser.add_argument("--top_p", type=float, default=0.9,
                        help="Nucleus filtering (top-p) before sampling (<=0.0: no filtering)")
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__file__)
    logger.info(pformat(args))


    if args.model_checkpoint == "":
        if args.model == 'gpt2':
            raise ValueError("Interacting with GPT2 requires passing a finetuned model_checkpoint")
        else:
            args.model_checkpoint = download_pretrained_model()

    if args.seed != 0:
        random.seed(args.seed)
        torch.random.manual_seed(args.seed)
        torch.cuda.manual_seed(args.seed)

    logger.info("Get pretrained model and tokenizer")
    tokenizer_class, model_class = (GPT2Tokenizer, GPT2LMHeadModel) if args.model == 'gpt2' else (OpenAIGPTTokenizer, OpenAIGPTLMHeadModel)
    tokenizer = tokenizer_class.from_pretrained(args.model_checkpoint)
    model = model_class.from_pretrained(args.model_checkpoint)
    model.to(args.device)
    add_special_tokens_(model, tokenizer)

    logger.info("Sample a personality")
    # ByMe
    obj, dataset = load_dataset_utils(tokenizer=tokenizer)  # tuple

    personalities = []
    for d_value in dataset['test']:
        personalities.append([d_value["facts"], d_value["attitudes"]])  # Add attitudes

    # Avoid empty list of facts
    for i in range(100):
        personality = random.choice(personalities)
        facts1 = personality[0]['depth_0']['first_speaker']
        facts2 = personality[0]['depth_0']['second_speaker']
        N = len(facts1)
        M = len(facts2)
        if N + M != 0:
            attitudes1 = personality[1]['depth_0']['first_speaker']
            attitudes2 = personality[1]['depth_0']['second_speaker']
            break
    if display:
        print("----------- Print Fact of first speaker ---------")
        print("Fact: {}".format(facts1))
        print("----------- Print Attitude first of speaker --------")
        print("Attitudes : {}".format(attitudes1))
        print("---------- Print Fact of second speaker -------")
        print("Fact: {}".format(facts2))
        print("---------- Print Attitude of second speaker -----")
        print("Attitudes : {}".format(attitudes2))
        print("--------- End of printig interact   ---------")

    # logger.info("Selected personality: %s", tokenizer.decode(personality))
    # logger.info("Selected personality: %s", tokenizer.decode(chain(*[personality[0],personality[1]])))

    to_file = []
    history = []
    counter = 0
    while counter < 10:
        counter += 1
        with torch.no_grad():
            out_ids = sample_sequence(obj, facts1, attitudes1, history, tokenizer, model, args)
        out_text = tokenizer.decode(out_ids, skip_special_tokens=True)
        history = history[-(2 * args.max_history + 1):]
        to_file.append(out_text)
        history.append(out_text)
        print("Speaker 1: {}".format(out_text))
        with torch.no_grad():
            out_ids = sample_sequence(obj, facts2, attitudes2, history, tokenizer, model, args)
        out_text = tokenizer.decode(out_ids, skip_special_tokens=True)
        history = history[-(2 * args.max_history + 1):]
        to_file.append(out_text)
        history.append(out_text)
        print("Speaker 2: {}".format(out_text))

    save_path = "interactions"
    file_name = args.model_checkpoint[5:-1]
    save_file = os.path.join(save_path, file_name)
    print(save_file)
    with open(save_file, "a+") as f:
        for utt in to_file:
            f.write(utt +"\n")
        f.close()
if __name__ == "__main__":
    run()
