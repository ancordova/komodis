#import torch.nn
import torch
import sys
from collections import defaultdict
from interact import top_filtering
#from torch.nn.modules.loss import CrossEntropyLoss
#from modeling_unlike_gpt2 import GPT2UnlikeModel
#from transformers import AdamW, GPT2Tokenizer
#from unlikelihood_train import get_data_loaders
#from moviecorpus import Moviecorpus

#From Fabian Galetzka to Everyone:  02:50 PM
#model1 = "GPT-LM-Head-Model"

# for the "new" approach
#model2 = "YourOwnGPT-LM-Head-Model"  # UnlikehoodTrainingGPT2Head

# 1. change get_torch_features so that it returns the wrong_candidates
# 2. create a new "Head"-Class from Transformer which implements the Unlikehoodtraining
# 3. For that, you need to create a new "Loss": The Unlikelihood-Loss

#a = torch.rand([4,1,255,502])
#b = a[0]
#print("b size: {}".format(b.size()))
#c = b[0, -1, :]
#print("c size: {}".format(c.size()))

#print(logits.size())
#labels = torch.load('labels.pt')

#a = logits[1][0][0]
#print(a)
#print(a.size())
#print("Maximum: {}".format(max(a)))
#print("Mainimum {}".format(min(a)))
#b = labels[0][0]
#print(b)
#print(b.size())

#t = torch.tensor([[1,2],[3,4]])
#r = torch.gather(t, 0, torch.tensor([[1,0],[1,1]]))
#print(r)

# a = torch.rand([4,1,25,50])
# b = torch.randint(0,10,[4,1,25])
# lm_loss_fct = CrossEntropyLoss()
# flat_shift_logits = a.view(-1, a.size(-1))
# print("flat_a: {}".format(flat_shift_logits.size()))
# flat_shift_labels = b.view(-1)
# print("flat_b: {}".format(flat_shift_labels.size()))
# lm_loss = lm_loss_fct(flat_shift_logits, flat_shift_labels)
# print(type(lm_loss))
# print("Ideal: Lm_Loss size: {}".format(lm_loss.size()))
# print(lm_loss)

#print(a.dtype)
#lprobs = torch.rand([1,25,50])
#print(lprobs.dtype)
#pred_lprobs = lprobs.view(-1, lprobs.size(2)).gather(1, a)
#print(pred_lprobs.size())

# class NGramIterator:
#     """
#     N-Gram iterator for a list.
#     """
#
#     def __init__(self, lst, n):
#         self.lst = lst
#         self.n = n
#         self.max = len(lst) - n
#
#     def __iter__(self):
#         self.counter = -1
#         return self
#
#     def __next__(self):
#         self.counter += 1
#         if self.counter > self.max:
#             raise StopIteration
#         return tuple(self.lst[self.counter : self.counter + self.n])
#
# lrep_mask = torch.zeros([4,1,256])
# i = 0
# n = 1
# gen_i = torch.randint(0,10,[50]).tolist()
# print("Gen 1: {}".format(gen_i))
# seen_n_grams = defaultdict(int)
# # penalize if there is a label repeat
# for j, n_gram in enumerate(NGramIterator(gen_i, n)):
#     print("Dict: {}".format(seen_n_grams))
#     print("N_gram: {}".format(n_gram))
#     print("--------------")
#     if seen_n_grams[n_gram] > 0:
#         print("¿Alguna vez entra aquí?")
#         lrep_mask[i, 0, j: j + n] = 1
#     seen_n_grams[n_gram] += 1
#     if j>5:
#         break

#print(torch.max(lrep_mask[0],dim=1))

# lm_logits = torch.load('logits.pt')
# #print(lm_logits[0][0].size())
# #print(torch.max(lm_logits[0][0]))
# #print(torch.min(lm_logits[0][0]))
# maxlen = 3
# batch_size = lm_logits.size(0)
# generations = []
# i = 0
# to_stack = []
# for j in range(maxlen):
#     to_stack.append(top_filtering(lm_logits[i][0,j,:]))
# tfcat = torch.stack((to_stack))
# generations.append(tfcat)
# #generations = [top_filtering(lm_logits[i][0,j,:]) for j in range(maxlen) for i in range(batch_size)]
# print(generations[0].size())
# print(generations[0])
# print("Máximo: {}".format(torch.max(generations[0])))
# print("Mínimo: {}".format(torch.min(generations[0])))
# candidates = generations[0] > 0
# print(candidates.size())

a = torch.tensor(([0,1,0,0,1],[1,0,1,0,1]))
print(a.sum())