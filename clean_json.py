import json

type_of_files = ['train', 'valid', 'test']

for type in type_of_files:
  old_file = type + "_dialogues.json"
  new_file = type + "_dialogues_mini.json"
  with open(old_file) as f:
    data = json.load(f)

  keys = []
  for key, value in data[0].items():
      keys.append(key)

  keys.remove("facts")
  keys.remove("facts_original")
  keys.remove("attitudes")
  keys.remove("dialogue_original")
  keys.remove("dialogue_ner_full")

  for element in data:
    for k in keys:
      if k in element:
        del element[k]

  with open(new_file, 'w') as nf:
      data = json.dump(data, nf)