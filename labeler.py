import os
import json
import numpy as np
from argparse import ArgumentParser

FILE_NAME = 'train'

class Labeler:

    def __init__(self):
        parser = ArgumentParser()
        parser.add_argument("--file", type=str, default="valid", help="Json file to classify: train, valid or test")
        args = parser.parse_args()
        self.file_prefix = args.file

    def get_example(self):
        original_json_path = '{}_dialogues.json'.format(self.file_prefix)
        with open(original_json_path, "r", encoding="utf-8") as f:
            valid_dataset = json.loads(f.read())

        N = len(valid_dataset)
        j = np.random.randint(0, N)
        example = valid_dataset[j]
        return example, j

    def classify(self):
        while True:
            example, j = self.get_example()
            self.display(example)
            print("        *           *          *           *          *           *          *           *")
            print("Please label this example")
            label = input(
                "0: Correct - 1: Repetition in Dialog - 2: Repetition of Facts - 3: Contradition - 4: Just Akward - q: Quit :           ")
            if label == "q":
                break
            self.save(self.file_prefix, j, label)


    def save(self,file_name, index, label):
        filename = '{}_classification.txt'.format(file_name)
        if not os.path.exists(filename):
            with open(filename, 'w'): pass
        f = open(filename, "a")
        f.write(label + ", " + str(index))
        f.close()

    def get_facts_and_attributes(self,example):
        facts = example['facts_original']
        first_facts = facts['first_speaker']
        second_facts = facts['second_speaker']
        attitudes = example['attitudes_original']
        first_attitudes = attitudes['first_speaker']
        second_attitudes = attitudes['second_speaker']
        return first_facts, second_facts, first_attitudes, second_attitudes

    def get_dialogue(self,example):
        dialogue = example['dialogue_original']
        return dialogue

    def display(self,example):
        facts = example['facts_original']
        first_facts = facts['first_speaker']
        second_facts = facts['second_speaker']
        attitudes = example['attitudes_original']
        first_attitudes = attitudes['first_speaker']
        second_attitudes = attitudes['second_speaker']
        dialogue = example['dialogue_original']

        print("------- Facts ---------")
        print("[Facts 1] ")
        for it in first_facts:
            object = it['object']
            relation = it['relation']
            subject = it['subject']
            message = "{} {}: {}".format(subject, relation, object)
            print(message)
        print("[Facts 2] ")
        for it2 in second_facts:
            object2 = it2['object']
            relation2 = it2['relation']
            subject2 = it2['subject']
            message2 = "{} {}: {}".format(subject2, relation2, object2)
            print(message2)

        print("------- Attitudes ---------")
        print("[Attitudes 1]")
        for it in first_attitudes:
            source = it['source']
            message = "{}".format(source)
            print(message)
        print("[Attitudes 2]")
        for it in second_attitudes:
            source = it['source']
            message = "{}".format(source)
            print(message)

        print("------- Dialogue ---------")
        i = 2
        for element in dialogue:
            if i % 2 == 0:
                SP = "[SP 1] "
            else:
                SP = "[SP 2] "
            print(SP + element)
            i += 1


if __name__ == "__main__":
    labeler = Labeler()
    labeler.classify()
