import json

original_json_path = 'valid_dialogues.json'
with open(original_json_path, "r", encoding="utf-8") as f:
    valid_dataset = json.loads(f.read())

def limit_json_n(dataset, n):
    i = 0
    result = []
    for d in dataset:
        i +=1
        if i>n:
            break
        result.append(d)
    final_str = json.dumps(result)
    #final_json = json.loads(final_str)
    return final_str

example = limit_json_n(valid_dataset,50)

# Writing to sample.json
with open("example.json", "w") as outfile:
    outfile.write(example)

#print("Example")
#print(example)
#print(type(example))
#print("Valid")
#print(valid_dataset[0])
#print(type(valid_dataset[0]))

example_json_path = 'example.json'
with open(example_json_path, "r", encoding="utf-8") as f:
    example_dataset = json.loads(f.read())

print("Example")
print(example_dataset[0])
print("Valid")
print(valid_dataset[0])