from transformers import add_start_docstrings
from transformers.models.gpt2.modeling_gpt2 import GPT2Model, GPT2PreTrainedModel, \
    PARALLELIZE_DOCSTRING, DEPARALLELIZE_DOCSTRING, GPT2_INPUTS_DOCSTRING
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.loss import CrossEntropyLoss
from transformers.models.gpt2.modeling_tf_gpt2 import add_start_docstrings_to_model_forward, add_code_sample_docstrings
from transformers.modeling_outputs import CausalLMOutputWithCrossAttentions
from transformers.utils.model_parallel_utils import get_device_map, assert_device_map
from transformers import GPT2Tokenizer
from collections import defaultdict
from moviecorpus import *
import sys
import time

def top_k_filtering(logits, top_k=1, threshold=0, filter_value=0): #-float('Inf')
    """ Filter a distribution of logits using top-k, and/or threshold filtering
    :param logits: logits distribution shape (vocabulary size)
    :param top_k: <=0: no filtering, >0: keep only top k tokens with highest probability.
    :param threshold: a minimal threshold to keep logits
    :param filter_value: The value that replaces the logits to remove
    :return logits: transformed, only containing value in the selected tokens
    """
    assert logits.dim() == 1  # Only work for batch size 1 for now - could update but it would obfuscate a bit the code
    top_k = min(top_k, logits.size(-1))
    if top_k > 0:
        # Remove all tokens with a probability less than the last token in the top-k tokens
        indices_to_remove = logits < torch.topk(logits, top_k)[0][..., -1, None]
        logits[indices_to_remove] = filter_value

    indices_to_remove = logits < threshold
    logits[indices_to_remove] = filter_value

    return logits

ATTR_TO_SPECIAL_TOKEN = {"bos_token": "<SST>", "eos_token": "<END>", "pad_token": "<PAD>",
                         "additional_special_tokens": ["<SPK:S>",  # DO NOT CHANGE THIS ONE!
                                                       "<SPK:O>",  # DO NOT CHANGE THIS ONE!
                                                       "<DEL:MOVIE>",
                                                       "<DEL:ACTOR0>",
                                                       "<DEL:ACTOR1>",
                                                       "<DEL:DIRECTOR>",
                                                       "<DEL:WRITER>",
                                                       "<DEL:YEAR>",
                                                       "<DEL:BUDGET>",
                                                       "<DEL:CERTIFICATE>",
                                                       "<DEL:COUNTRY>",
                                                       "<DEL:GENRE0>",
                                                       "<DEL:GENRE1>",
                                                       # "<DEL:CHARACTER>",
                                                       "<FACT:MOVIE>",
                                                       "<FACT:ACTOR0>",
                                                       "<FACT:ACTOR1>",
                                                       "<FACT:DIRECTOR>",
                                                       "<FACT:WRITER>",
                                                       "<FACT:PLOT>",
                                                       "<FACT:SUBJECT>",
                                                       "<FACT:OBJECT>",
                                                       "<OPINION:MOVIE>",
                                                       "<OPINION:ACTOR0>",
                                                       "<OPINION:ACTOR1>",
                                                       "<OPINION:DIRECTOR>",
                                                       "<OPINION:WRITER>",
                                                       "<OPINION:COUNTRY>",
                                                       "<OPINION:GENRE>",
                                                       "<OPINION:BUDGET>",
                                                       "<OPINION:CERTIFICATE>",
                                                       "<OPRATE:0>",
                                                       "<OPRATE:1>",
                                                       "<OPRATE:2>",
                                                       "<OPRATE:3>",
                                                       "<OPRATE:4>",
                                                       "<OPRATE:5>"]}

def add_special_tokens_(model, tokenizer):
    """ Add special tokens to the tokenizer and the model if they have not already been added. """
    orig_num_tokens = len(tokenizer.encoder)
    num_added_tokens = tokenizer.add_special_tokens(ATTR_TO_SPECIAL_TOKEN)  # doesn't add if they are already there
    if num_added_tokens > 0:
        model.resize_token_embeddings(new_num_tokens=orig_num_tokens + num_added_tokens)

class GPT2UnlikeModel(GPT2PreTrainedModel):
    _keys_to_ignore_on_load_missing = [r"h\.\d+\.attn\.masked_bias", r"lm_head\.weight"]

    def __init__(self, config):
        super().__init__(config)
        self.transformer = GPT2Model(config)
        self.lm_head = nn.Linear(config.n_embd, config.vocab_size, bias=False)

        self.init_weights()

        self.alpha = 20

        # Model parallel
        self.model_parallel = False
        self.device_map = None

    @add_start_docstrings(PARALLELIZE_DOCSTRING)
    def parallelize(self, device_map=None):
        self.device_map = (
            get_device_map(len(self.transformer.h), range(torch.cuda.device_count()))
            if device_map is None
            else device_map
        )
        assert_device_map(self.device_map, len(self.transformer.h))
        self.transformer.parallelize(self.device_map)
        self.lm_head = self.lm_head.to(self.transformer.first_device)
        self.model_parallel = True

    @add_start_docstrings(DEPARALLELIZE_DOCSTRING)
    def deparallelize(self):
        self.transformer.deparallelize()
        self.transformer = self.transformer.to("cpu")
        self.lm_head = self.lm_head.to("cpu")
        self.model_parallel = False
        torch.cuda.empty_cache()

    def get_output_embeddings(self):
        return self.lm_head

    def set_output_embeddings(self, new_embeddings):
        self.lm_head = new_embeddings

    def prepare_inputs_for_generation(self, input_ids, past=None, **kwargs):
        token_type_ids = kwargs.get("token_type_ids", None)
        # only last token for inputs_ids if past is defined in kwargs
        if past:
            input_ids = input_ids[:, -1].unsqueeze(-1)
            if token_type_ids is not None:
                token_type_ids = token_type_ids[:, -1].unsqueeze(-1)

        attention_mask = kwargs.get("attention_mask", None)
        position_ids = kwargs.get("position_ids", None)

        if attention_mask is not None and position_ids is None:
            # create position_ids on the fly for batch generation
            position_ids = attention_mask.long().cumsum(-1) - 1
            position_ids.masked_fill_(attention_mask == 0, 1)
            if past:
                position_ids = position_ids[:, -1].unsqueeze(-1)
        else:
            position_ids = None
        return {
            "input_ids": input_ids,
            "past_key_values": past,
            "use_cache": kwargs.get("use_cache"),
            "position_ids": position_ids,
            "attention_mask": attention_mask,
            "token_type_ids": token_type_ids,
        }

    @add_start_docstrings_to_model_forward(GPT2_INPUTS_DOCSTRING)
    @add_code_sample_docstrings(
        tokenizer_class="GPT2Tokenizer",
        checkpoint="gpt2",
        output_type=CausalLMOutputWithCrossAttentions,
        config_class="GPT2Config",
    )

    def forward(
            self,
            input_ids=None,
            past_key_values=None,
            attention_mask=None,
            token_type_ids=None,
            position_ids=None,
            head_mask=None,
            inputs_embeds=None,
            encoder_hidden_states=None,
            encoder_attention_mask=None,
            labels=None,
            use_cache=None,
            output_attentions=None,
            output_hidden_states=None,
            return_dict=None,
    ):
        r"""
        labels (:obj:`torch.LongTensor` of shape :obj:`(batch_size, sequence_length)`, `optional`):
            Labels for language modeling. Note that the labels **are shifted** inside the model, i.e. you can set
            ``labels = input_ids`` Indices are selected in ``[-100, 0, ..., config.vocab_size]`` All labels set to
            ``-100`` are ignored (masked), the loss is only computed for labels in ``[0, ..., config.vocab_size]``
        """
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict
        torch.autograd.set_detect_anomaly(True)

        transformer_outputs = self.transformer(
            input_ids,
            past_key_values=past_key_values,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            encoder_hidden_states=encoder_hidden_states,
            encoder_attention_mask=encoder_attention_mask,
            use_cache=use_cache,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )
        hidden_states = transformer_outputs[0]

        # Set device for model parallelism
        if self.model_parallel:
            torch.cuda.set_device(self.transformer.first_device)
            hidden_states = hidden_states.to(self.lm_head.weight.device)

        tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
        add_special_tokens_(self, tokenizer)
        #print("Type inputs ids: {}".format(type(input_ids)))
        #print("Size of inputs ids: {}".format(input_ids.size()))

        lm_logits = self.lm_head(hidden_states) # [4,1,256,50296]
        #print("Logits size : {}".format(lm_logits.size()))
        #torch.save(lm_logits,"logits.pt")
        #torch.save(labels,"labels.pt")

        loss = None
        lm_loss = None
        if labels is not None:

            ### CALCULATING NORMAL LOSS
            # Shift so that tokens < n predict n
            shift_logits = lm_logits[..., :-1, :].contiguous()
            #print("Shift_logits size : {}".format(shift_logits.size())) # [4,1,255,50296]
            shift_labels = labels[..., 1:].contiguous()
            #print("Shift_labels size : {}".format(shift_labels.size())) # [4,1,255]
            # Flatten the tokens
            lm_loss_fct = CrossEntropyLoss()
            flat_shift_logits = shift_logits.view(-1, shift_logits.size(-1))
            flat_shift_labels = shift_labels.view(-1)
            lm_loss = lm_loss_fct(flat_shift_logits, flat_shift_labels)

            batch_size = lm_logits.size(0)
            maxlen = lm_logits.size(2)
            vocab_size = lm_logits.size(3)

            lm_logits = lm_logits.view(batch_size,maxlen,vocab_size)
            # construct mask marking repeats
            lrep_mask = torch.zeros_like(lm_logits).type_as(lm_logits) # [4, 256, 50296]

            with torch.no_grad():
                generations = []
                for i in range(batch_size):
                    to_stack = []
                    for j in range(maxlen):
                        to_stack.append(top_k_filtering(lm_logits[i][j, :],top_k=1))
                    logits_filtered = torch.stack((to_stack))
                    generations.append(logits_filtered)

            for i, gen in enumerate(generations): # Loop over samples
                seen_n_grams = defaultdict(int)
                out_seq = ""
                for j, seq in enumerate(gen): # Loop over sequences
                    predicted = seq.nonzero(as_tuple=True)[0].tolist()
                    pred = predicted[0]
                    if i == 0:
                        out_pred = tokenizer.decode(predicted, skip_special_tokens=True)
                        out_seq = out_seq + out_pred
                    if seen_n_grams[pred] > 0 and pred < 50255:
                        lrep_mask[i, j, pred] = 1
                    seen_n_grams[pred] += 1
            lprobs = F.softmax(lm_logits, dim=-1)
            selected_lprobs = lprobs * lrep_mask
            clamp_min = 1e-20
            one_minus_probs = torch.clamp((1.0 - selected_lprobs), min=clamp_min)

            ul_loss = -(torch.log(one_minus_probs))
            unlike_loss = ul_loss.sum()

            loss = lm_loss + self.alpha * unlike_loss

        if not return_dict:
            output = (lm_logits,) + transformer_outputs[1:]
            return ((loss,) + output) if loss is not None else output

        return CausalLMOutputWithCrossAttentions(
            loss=loss,
            logits=lm_logits,
            past_key_values=transformer_outputs.past_key_values,
            hidden_states=transformer_outputs.hidden_states,
            attentions=transformer_outputs.attentions,
            cross_attentions=transformer_outputs.cross_attentions,
        )