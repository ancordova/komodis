""" Handles the moviecorpus data.

"""
import os
import sys
import json
import copy
import pickle
import random
import pathlib
import numpy as np
from tqdm import tqdm
import transformers
from transformers import GPT2Tokenizer

from itertools import chain
from argparse import Namespace
from configparser import ConfigParser
#from include.sentence_patterns import AttitudesV2

import torch
from torch.utils.data import DataLoader, TensorDataset

SPECIAL_TOKENS = ["<SST>", "<END>", "<PAD>", "<SPK:S>", "<SPK:O>", "<DEL:MOVIE>", "<DEL:ACTOR>", "<DEL:DIRECTOR>",
                  "<DEL:WRITER>", "<DEL:YEAR>", "<DEL:BUDGET>", "<DEL:CERTIFICATE>", "<DEL:COUNTRY>", "<DEL:GENRE0>",
                  "<DEL:GENRE1>", "<FACT:MOVIE>", "<FACT:ACTOR0>", "<FACT:ACTOR1>", "<FACT:DIRECTOR>", "<FACT:WRITER>",
                  "<FACT:PLOT>", "<FACT:SUBJECT>", "<FACT:OBJECT>", "<OPINION:MOVIE>", "<OPINION:ACTOR>",
                  "<OPINION:DIRECTOR>", "<OPINION:WRITER>", "<OPINION:COUNTRY>", "<OPINION:GENRE>", "<OPINION:BUDGET>",
                  "<OPINION:CERTIFICATE>", "<OPRATE:0>", "<OPRATE:1>", "<OPRATE:2>", "<OPRATE:3>", "<OPRATE:4>",
                  "<OPRATE:5>"]
ATTR_TO_SPECIAL_TOKENS = {"bos_token": "<SST>", "eos_token": "<END>", "pad_token": "<PAD>",
                          "additional_special_tokens": ["<SPK:S>",  # DO NOT CHANGE THIS ONE!
                                                        "<SPK:O>",  # DO NOT CHANGE THIS ONE!
                                                        "<DEL:MOVIE>",
                                                        "<DEL:ACTOR0>",
                                                        "<DEL:ACTOR1>",
                                                        "<DEL:DIRECTOR>",
                                                        "<DEL:WRITER>",
                                                        "<DEL:YEAR>",
                                                        "<DEL:BUDGET>",
                                                        "<DEL:CERTIFICATE>",
                                                        "<DEL:COUNTRY>",
                                                        "<DEL:GENRE0>",
                                                        "<DEL:GENRE1>",
                                                        # "<DEL:CHARACTER>",
                                                        "<FACT:MOVIE>",
                                                        "<FACT:ACTOR0>",
                                                        "<FACT:ACTOR1>",
                                                        "<FACT:DIRECTOR>",
                                                        "<FACT:WRITER>",
                                                        "<FACT:PLOT>",
                                                        "<FACT:SUBJECT>",
                                                        "<FACT:OBJECT>",
                                                        "<OPINION:MOVIE>",
                                                        "<OPINION:ACTOR0>",
                                                        "<OPINION:ACTOR1>",
                                                        "<OPINION:DIRECTOR>",
                                                        "<OPINION:WRITER>",
                                                        "<OPINION:COUNTRY>",
                                                        "<OPINION:GENRE>",
                                                        "<OPINION:BUDGET>",
                                                        "<OPINION:CERTIFICATE>",
                                                        "<OPRATE:0>",
                                                        "<OPRATE:1>",
                                                        "<OPRATE:2>",
                                                        "<OPRATE:3>",
                                                        "<OPRATE:4>",
                                                        "<OPRATE:5>"]}

TOKEN_ENCODING_MAPPING = {
    "movie#0": "<DEL:MOVIE>",
    "actor#0": "<DEL:ACTOR0>",
    "actor#1": "<DEL:ACTOR1>",
    "director#0": "<DEL:DIRECTOR>",
    "writer#0": "<DEL:WRITER>",
    "year#0": "<DEL:YEAR>",
    "budget#0": "<DEL:BUDGET>",
    "certificate#0": "<DEL:CERTIFICATE>",
    "country#0": "<DEL:COUNTRY>",
    "genre#0": "<DEL:GENRE0>",
    "genre#1": "<DEL:GENRE1>",
    "character#0": "<DEL:CHARACTER>"
}

KG_ENCODING_MAPPING = {
    "movie": "<DEL:MOVIE>",
    "actor": "<DEL:ACTOR0>",
    "person": "<DEL:ACTOR1>",
    "writer": "<DEL:WRITER>",
    "director": "<DEL:DIRECTOR>",
    "role": "<FACT:ACTOR0>",
    "age restriction": "<DEL:CERTIFICATE>",
    "certificate": "<DEL:CERTIFICATE>",
    "budget": "<DEL:BUDGET>",
    "shot location": "<DEL:COUNTRY>",
    "release year": "<DEL:YEAR>",
    "genre": "<DEL:GENRE0>",
    "plot": "<FACT:PLOT>",
    "trivia": "<FACT:OBJECT>",
    "attitude": "<OPINION:MOVIE>",
    "random_attitude": "<OPINION:MOVIE>"
}

GRAPH_DEPTH_DICT = {
    0: "dialogue",
    1: "dialogue_full_movie",
    2: "dialogue_second_movie",
    3: "dialogue_full_movie_plus_one_hop",
    4: "dialogue_second_movie_plus_one_hop"
}

OPINION_RELATION_TO_TRIPLE_DICT = {
    "has_general_bot_attitude": "attitude",
    "has_bot_certificate_attitude": "certificate attitude"
}

OPINION_OBJECT_TO_TRIPLE_DICT = {
    0: "don't know",
    1: "hate",
    2: "don't like",
    3: "like",
    4: "love",
    5: "favorite"
}

OPINION_OBJECT_TO_TRIPLE_DICT_AGE_RESTR = {
    2: ["disagree", "too high", "too low"],
    3: ["agree"]
}
IVERSE_OBJECT_TO_TRIPLE_AGE_RESTR = {
    "disagree": 2,
    "too high": 2,
    "too low": 2,
    "agree": 3,
}

FACT_RELATION_GRAPH_TRIPLE_DICT = {
    "actor": "has_actor",
    "director": "has_director",
    "writer": "has_writer",
    "age restriction": "has_age_certificate",
    "genre": "has_genre",
    "shot location": "has_shot_location",
    "plot": "has_plot",
    "release year": "has_release_year",
    "budget": "has_budget",
    "trivia": "has_trivia",
    "role": "has_role",
    "has_character": "character"
}

TRIPLE_OPINION_SUBJECT_TYPE_DICT = {
    "actor": "actor#0",
    "director": "director#0",
    "writer": "writer#0",
    "age restriction": "certificate#0",
    "genre": "genre#0",
    "shot location": "country#0",
    "release year": "year#0",
    "movie": "movie#0"
}

DEFAULT_HPS = {
    "remove_delex": True, # Never change
    "attitude_sentences": True,
    "difficult_wrong_candidates": False,
    "kg_encoding": False,
    "kg_encoding_type": "smart",  # one of: komodis, edge_node, smart
    "graph_depth": 0,
    "context_paraphrased": True, # Can experiment
    "context_triples": False,
    "use_graphs_as_triples": False, # Never change
    "max_context_length": 256,
    "max_input_length": 256,
    "same_movie_candidates":False,
    #"debug":False,
}


def create_fact_sentence(subj, relation, obj, separate_komma=True):
    """ """
    if relation == "has_genre":
        sentence = "{} is of genre {}.".format(subj, obj)
    elif relation == "has_release_year":
        sentence = "{} was released in {}.".format(subj, obj)
    elif relation == "has_budget":
        if separate_komma:
            sentence = "{} had a budget of {}.".format(subj, obj).replace(",", ", ")
        else:
            sentence = "{} had a budget of {}.".format(subj, obj)
    elif relation == "has_shot_location":
        sentence = "{} was shot in {}.".format(subj, obj)
    elif relation == "has_actor":
        sentence = "{} is an actor in {}.".format(obj, subj)
    elif relation == "has_writer":
        sentence = "{} is the writer of {}.".format(obj, subj)
    elif relation == "has_director":
        sentence = "{} is the director of {}.".format(obj, subj)
    elif relation == "has_age_certificate":
        sentence = "{} is certified for persons of age {} and older.".format(subj, obj)
    elif relation == "has_role":
        sentence = "Actor {} plays {} in the movie.".format(subj, obj)
    elif relation == "has_plot":
        sentence = "{} has plot: {}".format(subj, obj)
    elif relation == "has_trivia":
        sentence = "{} has trivia: {}.".format(subj, obj)
    else:
        raise ValueError("{} not found.".format(relation))
    return sentence


def get_sub_graph_by_id(sub_graphs, graph_depth):
    """ """
    for sub_graph in sub_graphs:
        if sub_graph["depth"] == GRAPH_DEPTH_DICT[graph_depth]:
            return sub_graph
    raise ValueError("No sub_graph with with graph_depth = {}".format(graph_depth))


class Moviecorpus:
    """ Class to read and process the KOMODIS dataset.

    Read raw data with load_txt_dataset(...).
    Tokenize and prepare for training with tokenize_dataset(...)
    Create a dataset for PyTorch training with get_torch_features(...)
    """
    def __init__(self, path_to_data="", hps=None, tokenizer=None, debug=False, test=False):
        """
        Args:
            path_to_data    A string. Location of the raw datafile.
            hps             A Namespace object. Parameter for training.
            tokenizer       A Tokenizer object from transformers.
            debug           A Boolean. If True, only a small dataset is processed; additional print calls.
            test            A Boolean. If True, the dataset will be prepared for testing a trained model.

        """
        self.path_to_data = path_to_data
        self.hps = hps if hps is not None else Namespace(**DEFAULT_HPS)
        self.hps.test = test
        self.debug = debug
        self.hps.debug = debug #ByMe

        self.num_added_tokens = None
        self.special_tokens_dict = None
        self.kg_encoding_mapping = None
        self.dataset = None
        self.tokenizer = None
        self.new_dataset_encoding_v9 = None

        if tokenizer is not None:
            self.prepare_tokenizer(tokenizer=tokenizer)

        # check some constraints
        if self.hps.remove_delex and not self.hps.attitude_sentences:
            # For the first shot the removed delexicalisation will only work for attitude sentences, since we
            # don't know how to encode knowledge-triples where nodes have more then one token (e.g. Tom Hanks).
            if not self.hps.context_triples:
                print("WARNING: Removing the delexicalisation requires attitude_sentences to be enabled!")
                self.hps.attitude_sentences = True
                print("WARNING: attitude_sentences attribute set to TRUE!")

        if self.tokenizer is not None:
            # add special tokens (only used without delexicalization)
            # Most of the Delex-Tokens are used differently, since we don't need them anymore!
            self._t_fact_subject = self.tokenizer.encode("<FACT:SUBJECT>")[0]
            self._t_fact_body = self.tokenizer.encode("<FACT:OBJECT>")[0]
            self._t_fact_plot = self.tokenizer.encode("<FACT:PLOT>")[0]
            self._t_fact_actor = self.tokenizer.encode("<DEL:ACTOR0>")[0]
            self._t_fact_writer = self.tokenizer.encode("<DEL:WRITER>")[0]
            self._t_fact_director = self.tokenizer.encode("<DEL:DIRECTOR>")[0]
            self._t_fact_year = self.tokenizer.encode("<DEL:YEAR>")[0]
            self._t_fact_genre = self.tokenizer.encode("<DEL:GENRE0>")[0]
            self._t_fact_budget = self.tokenizer.encode("<DEL:BUDGET>")[0]
            self._t_fact_certificate = self.tokenizer.encode("<DEL:CERTIFICATE>")[0]
            self._t_fact_location = self.tokenizer.encode("<DEL:COUNTRY>")[0]

    def prepare_tokenizer(self, tokenizer=None):
        """ Initializes a tokenizer.

        Only use this function once and if you haven't specified a Tokenizer at instance creation.
        """
        if self.tokenizer is not None:
            raise Exception("Tokenizer already initialized.")
        self.tokenizer = tokenizer
        self.num_added_tokens = self.tokenizer.add_special_tokens(ATTR_TO_SPECIAL_TOKENS)
        ids = self.tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS[3:])
        self.special_tokens_dict = dict(zip(SPECIAL_TOKENS[3:], ids))
        self.kg_encoding_mapping = {}
        for key, value in KG_ENCODING_MAPPING.items():
            value_id = self.tokenizer.convert_tokens_to_ids(value)
            self.kg_encoding_mapping[key] = value_id

    def load_txt_dataset(self, train=True, valid=True, test=True, sub_graphs=False):
        """ Loads the raw dataset.

        For the moviecorpus it is pretty straight-forward as the data is already processed.
        """
        # check for old dataset
        if os.path.isfile(self.path_to_data):
            self.new_dataset_encoding_v9 = False
            # old one: "raw/moviechat_dataset_20200324_paper.json"
            with open(self.path_to_data, "r") as f:
                data = json.load(f)
            print("WARNING: The given dataset has an old format. Training and tokenization may fail!")
            # split into train, validation and test.
            if isinstance(data, list):
                self.dataset = Moviecorpus._split_binarized_corpus(data)
            else:
                self.dataset = data
        # load new dataset (if available)
        else:
            self.new_dataset_encoding_v9 = True
            self.dataset = {}
            for split, split_requested in zip(["train", "valid", "test"], [train, valid, test]):
                if split_requested:
                    with open(os.path.join(self.path_to_data, "{}_dialogues_mini.json".format(split)), "r") as f:
                        self.dataset[split] = json.load(f)
                if split_requested and sub_graphs:
                    with open(os.path.join(self.path_to_data, "{}_graphs.json".format(split)), "r") as f:
                        sub_graphs_data = json.load(f)
                    for dials, graphs in zip(self.dataset[split], sub_graphs_data):
                        dials["sub_graphs"] = graphs
                        if dials["dialogue_id"] != graphs[0]["dialogue_id"]:
                            raise ValueError("Processing Error! Check graphs list for {} data!".format(split))

    def tokenize_dataset(self, path_to_save=None):
        """ Prepares the dataset for training.

        Args:
             path_to_save   A string. If not None the tokenized dataset is stored on disk.
        """
        assert self.dataset is not None  # Dataset need to be loaded first!
        assert self.tokenizer is not None  # A tokenizer is needed for tokenization!

        if "use_graphs_as_triples" not in self.hps:
            self.hps.use_graphs_as_triples = False
        if "graph_depth" not in self.hps:
            self.hps.graph_depth = 0
        if "context_paraphrased" not in self.hps:
            self.hps.context_paraphrased = False
        if "context_triples" not in self.hps:
            self.hps.context_triples = False
        if "max_context_length" not in self.hps:
            self.hps.max_context_length = -1

        for split, dialogues in self.dataset.items():
            # if test mode, skip all but "test":
            if self.hps.test and split != "test":
                continue
            print("[Tokenize] Processing {} data ...".format(split))
            for num, dialogue in enumerate(tqdm(dialogues)):
                # --- binarize dialogue -------------------------------------------------------------------------------
                if self.hps.remove_delex:
                    dialogue["dialogue_processed"] = [d.lower() for d in dialogue["dialogue_original"]]

                else:
                    dialogue["dialogue_processed"] = Moviecorpus._replace_special_tokens(dialogue["dialogue_ner_full"])
                dialogue["dialogue_processed"] = \
                    Moviecorpus._replace_special_moviecorpus_tokens(dialogue["dialogue_processed"])
                dialogue["dialogue_binarized"] = [self.tokenizer.encode(d) for d in dialogue["dialogue_processed"]]

                # --- binarize wrong utterances -----------------------------------------------------------------------
                if self.hps.difficult_wrong_candidates:
                    dialogue["wrong_candidates_processed"] = []
                    if self.hps.remove_delex:
                        items = dialogue["wrong_candidates_orig"][1:]
                    else:
                        items = dialogue["wrong_candidates_ner"][1:]
                    for item in items:
                        d = {}
                        for key, utterances in item.items():
                            temp = [x.lower() for x in utterances]
                            temp = Moviecorpus._replace_special_tokens(temp)
                            temp = Moviecorpus._replace_special_moviecorpus_tokens(temp)
                            d[key] = [self.tokenizer.encode(x) for x in temp]
                        dialogue["wrong_candidates_processed"].append(d)
                # --- convert subgraphs into triples, if needed -------------------------------------------------------
                if self.hps.use_graphs_as_triples:
                    new_facts = {}
                    new_opinions = {}
                    for speaker in ["first_speaker", "second_speaker"]:
                        new_facts[speaker], new_opinions[speaker] = \
                            self.convert_graph_to_triples(dialogue["sub_graphs"][self.hps.graph_depth][speaker])
                    dialogue["facts"]["depth_{}".format(self.hps.graph_depth)] = new_facts
                    dialogue["attitudes"]["depth_{}".format(self.hps.graph_depth)] = new_opinions
                # --- binarize facts and attitudes or sub-graph -------------------------------------------------------
                if self.hps.kg_encoding:
                    sub_graph = get_sub_graph_by_id(dialogue["sub_graphs"], self.hps.graph_depth)
                    dialogue["kg_encoded"] = self._process_subgraph(sub_graph)
                else:
                    # --- process facts -------------------------------------------------------------------------------
                    # downward compatibility check
                    if "depth_0" not in dialogue["facts"]:
                        if self.hps.graph_depth == 0:
                            print("INFO: Your dataset has version 9 or lower. Please use the newest version as "
                                  "soon as possible!")
                        else:
                            print("ERROR: graph_depth is higher than 0. The current dataset version is not compatible "
                                  "with that. Use dataset version 10 or higher!")
                            sys.exit()
                        if self.hps.remove_delex:
                            dialogue["facts_binarized"] = self._process_and_binarize_facts(dialogue["facts_original"])
                        else:
                            dialogue["facts_binarized"] = self._process_and_binarize_facts(dialogue["facts"])
                    else:
                        # --- handling of new dataset version (10 or higher)
                        depth = "depth_{}".format(self.hps.graph_depth)
                        dialogue["facts_binarized"] = self._process_and_binarize_facts_v2(dialogue["facts"][depth])

                    # --- process attitudes ---------------------------------------------------------------------------
                    if self.new_dataset_encoding_v9:
                        depth = "depth_{}".format(self.hps.graph_depth)
                        if "first_speaker" in dialogue["attitudes"]:
                            dialogue["attitudes"]["depth_0"] = \
                                {"first_speaker": dialogue["attitudes"]["first_speaker"],
                                 "second_speaker": dialogue["attitudes"]["second_speaker"]}
                        dialogue["attitudes_binarized"] = \
                            self._process_and_binarize_attitudes_v2(dialogue["attitudes"][depth],
                                                                    attitude_sentences=self.hps.attitude_sentences)
                    else:
                        # --- OLD ATTITUDE PROCESSING -----------------------------------------------------------------
                        if self.hps.remove_delex:
                            # we combine attitudes_originals source with the processed attitudes here
                            merged_attitudes = Moviecorpus._merge_attitudes_for_remove_delex(
                                original=dialogue["attitudes_original"],
                                processed=dialogue["attitudes"]
                            )
                            dialogue["attitudes_binarized"] = \
                                self._process_and_binarize_attitudes(merged_attitudes,
                                                                     attitude_sentences=self.hps.attitude_sentences)
                        else:
                            dialogue["attitudes_binarized"] = \
                                self._process_and_binarize_attitudes(dialogue["attitudes"],
                                                                     attitude_sentences=self.hps.attitude_sentences)
                        # ---------------------------------------------------------------------------------------------

                if self.debug and num > 256:
                    break

        # --- saving data ---
        if path_to_save is not None:
            path = os.path.join(self.path_to_data, path_to_save)
            if not os.path.exists(os.path.dirname(path)):
                os.mkdir(os.path.dirname(path))
            with open(path, "wb") as f:
                pickle.dump(self.dataset, f)

    def get_torch_features(self, split, batch_size, num_wrong_utts=0, distributed=False):
        """ Creates torch features for training. """
        # load binarized data (or binarize if not binarized)
        if "dialogue_binarized" not in self.dataset[split][0]:
            self.tokenize_dataset()
        samples = self._convert_dialogues_to_samples(split=split, num_wrong_utts=num_wrong_utts)

        # create features
        features = {
            "input_ids": [],
            "mc_token_ids": [],
            "lm_labels": [],
            "mc_labels": [],
            "token_type_ids": [],
            "kg_attn_matrix": None,
        }
        if self.hps.kg_encoding:
            features["kg_attn_matrix"] = []

        #if self.hps.debug:
        #    print("*** MULTIPLE CHOICE TASK EXAMPLES ***")

        for n, sample in enumerate(samples):
            num_wrongs = 0
            true_answer_id = int(np.random.rand() * (1 + num_wrong_utts))
            debug_utterances_to_print = []

            for num in range(1 + num_wrong_utts):
                if num == true_answer_id:
                    seqs = self._convert_sample_to_sequences(facts=sample["facts"],
                                                             attitudes=sample["attitudes"],
                                                             history=sample["dialogue_history"],
                                                             reply=sample["label_utterance"][0],
                                                             lm_labels=True)
                    #if self.hps.debug:
                    #    debug_utterances_to_print.append(self.tokenizer.decode(sample["label_utterance"][0]))
                    if self.hps.kg_encoding:
                        features["kg_attn_matrix"].append(seqs["kg_attn_matrix"])
                else:
                    seqs = self._convert_sample_to_sequences(facts=sample["facts"],
                                                             attitudes=sample["attitudes"],
                                                             history=sample["dialogue_history"],
                                                             reply=sample["wrong_utterances"][num_wrongs],
                                                             lm_labels=False)
                    if self.hps.debug:
                        debug_utterances_to_print.append(self.tokenizer.decode(sample["wrong_utterances"][num_wrongs]))
                    num_wrongs += 1

                features["input_ids"].append(seqs["input_ids"])
                features["token_type_ids"].append(seqs["token_type_ids"])
                features["mc_token_ids"].append(seqs["mc_token_ids"])
                features["lm_labels"].append(seqs["lm_labels"])
                features["mc_labels"].append(true_answer_id)
                if self.hps.kg_encoding:
                    features["kg_attn_matrix"].append(seqs["kg_attn_matrix"])

            if self.hps.debug and n in [7, 11, 21, 31, 33, 41, 60, 65]:
                print(" ")
                print("History:")
                for s in sample["dialogue_history"]:
                    print(" > '{}'".format(self.tokenizer.decode(s)))
                print("Candidates:")
                for s in debug_utterances_to_print:
                    print(s)
                print("mc label: {}".format(true_answer_id))
                print("---")

        # padding
        features_padded = self._pad_features(features, padding=self.tokenizer.pad_token_id)

        features_combined = {
            "input_ids": [],
            "mc_token_ids": [],
            "lm_labels": [],
            "mc_labels": [],
            "token_type_ids": [],
        }
        if self.hps.kg_encoding:
            features_combined["kg_attn_matrix"] = []

        for num in tqdm(range(int(len(features["input_ids"])/(1 + num_wrong_utts)))):
            sst = num * (1 + num_wrong_utts)
            end = sst + 1 + num_wrong_utts
            input_ids = features_padded["input_ids"][sst:end]
            token_type_ids = features_padded["token_type_ids"][sst:end]

            lm_labels = features_padded["lm_labels"][sst:end]
            if num_wrong_utts > 0:
                mc_labels = features_padded["mc_labels"][sst]
                mc_token_ids = features_padded["mc_token_ids"][sst:end]
            else:
                mc_labels = 0
                mc_token_ids = []
            if self.hps.kg_encoding:
                kg_attn_matrix = features_padded["kg_attn_matrix"][num]
                features_combined["kg_attn_matrix"].append(kg_attn_matrix)
            features_combined["input_ids"].append(input_ids)
            features_combined["token_type_ids"].append(token_type_ids)
            features_combined["mc_token_ids"].append(mc_token_ids)
            features_combined["lm_labels"].append(lm_labels)
            features_combined["mc_labels"].append(mc_labels)

        torch_features = []
        for key, value in features_combined.items():
            torch_features.append(torch.tensor(value))
        dataset = TensorDataset(*torch_features)

        if distributed:
            sampler = torch.utils.data.distributed.DistributedSampler(dataset)
        else:
            sampler = None

        if distributed or split != "train":
            shuffle = False
        else:
            shuffle = True

        loader = DataLoader(dataset, sampler=sampler, batch_size=batch_size, shuffle=shuffle, drop_last=True)

        return sampler, loader

    def get_reinforcement_learning_sequences(self, split):
        """ Generates reinforcement learning samples. """
        samples = self._convert_dialogues_to_samples(split=split, num_wrong_utts=0)
        original = self.dataset[split]

        sequences = []
        for sample in samples:
            seq = self._convert_sample_to_sequences(facts=sample["facts"],
                                                    attitudes=sample["attitudes"],
                                                    history=sample["dialogue_history"],
                                                    reply=sample["label_utterance"][0],
                                                    lm_labels=True)
            seq["facts"] = [x[2] for x in sample["facts"]]
            seq["opinions"] = [x[2] for x in sample["attitudes"]]
            sequences.append(seq)
        return sequences

    def load_dataset(self, path):
        """ Loads a (already binarized) dataset into the object. """
        with open(path, "rb") as f:
            ds = pickle.load(f)
            for split in ["train", "valid", "test"]:
                if split not in ds:
                    raise Exception("Dataset does not contain {} data.".format(split))
                if "dialogue_binarized" not in ds[split][0]:
                    raise Exception("Data is not binarized!")
            self.dataset = ds

    def _process_and_binarize_facts(self, facts, inference=False):
        """   """
        def create_seqs_with_delex():
            """ The sequences if delexicalisation is used """
            if fact["relation"] in ["has_trivia", "has_plot"]:
                facts_processed[speaker].append((
                    fact_tokenized,
                    len_fact * self.tokenizer.encode(token)
                ))

        def create_seqs_without_delex():
            """ Sequences if delexicalisation is not used """
            if fact["relation"] == "has_trivia":  # TODO: add some more
                subject_tokenized = self.tokenizer.encode(fact["subject"])
                len_subject = len(subject_tokenized)

                facts_processed[speaker].append((
                    subject_tokenized + fact_tokenized,
                    len_subject * [self._t_fact_subject] + len_fact * [self._t_fact_body]
                ))
                return
            elif fact["relation"] == "has_plot":
                facts_processed[speaker].append((
                    fact_tokenized,
                    len_fact * [self._t_fact_plot]
                ))
                return
            elif fact["relation"] == "has_role":
                # TODO: has_role is movie-title + role NOT actor :-( change this!!!
                return
                subject_tokenized = self.tokenizer.encode(fact["subject"])
                len_subject = len(subject_tokenized)
                fact_tokenized_for_role = self.tokenizer.encode("plays the character {} .".format(fact["object"]))
                len_fact_tokenized_for_role = len(fact_tokenized_for_role)
                facts_processed[speaker].append((
                    subject_tokenized + fact_tokenized_for_role,
                    len_subject * self._t_fact_subject + len_fact_tokenized_for_role * self._t_fact_body
                ))
                stop = "here"
                # return
            elif fact["relation"] == "has_budget":
                body_token = self._t_fact_budget
            elif fact["relation"] == "has_age_certificate":
                body_token = self._t_fact_certificate
            elif fact["relation"] == "has_shot_location":
                body_token = self._t_fact_location
            elif fact["relation"] == "has_genre":
                body_token = self._t_fact_genre
            elif fact["relation"] == "has_release_year":
                body_token = self._t_fact_year
            elif fact["relation"] == "has_actor":
                body_token = self._t_fact_actor
            elif fact["relation"] == "has_writer":
                body_token = self._t_fact_writer
            elif fact["relation"] == "has_director":
                body_token = self._t_fact_director
            else:
                raise Exception("{} not implemented".format(fact["relation"]))
            facts_processed[speaker].append((
                fact_tokenized,
                len_fact * [body_token]
            ))

        if inference:
            facts_processed = {"inference": []}
            speakers = ["inference"]
            facts = {"inference": facts}
        else:
            facts_processed = {"first_speaker": [], "second_speaker": []}
            speakers = ["first_speaker", "second_speaker"]

        for speaker in speakers:
            ss_facts = facts[speaker]
            for fact in ss_facts:
                fact_tokenized = self.tokenizer.encode(fact["object"])  # TODO: _replace_special_tokens needed?
                len_fact = len(fact_tokenized)
                token = Moviecorpus._get_correct_fact_token(subject=fact["subject"], relation=fact["relation"])
                if self.hps.remove_delex:
                    create_seqs_without_delex()
                else:
                    create_seqs_with_delex()

        if inference:
            facts_processed = facts_processed["inference"]

        return facts_processed

    def _process_and_binarize_facts_v2(self, facts, inference=False):
        """   """
        def create_seqs_with_delex():
            """ The sequences if delexicalisation is used """
            if fact["relation"] in ["has_trivia", "has_plot"]:
                facts_processed[speaker].append((
                    fact_tokenized,
                    len_fact * self.tokenizer.encode(token),
                    fact
                ))

        def create_seqs_without_delex_v2():
            facts_processed[speaker].append((
                fact_tokenized,
                len_fact * self.tokenizer.encode(token),
                fact
            ))

        def create_seqs_without_delex():
            """ Sequences if delexicalisation is not used """
            if fact["relation"] == "has_trivia":
                subject_tokenized = self.tokenizer.encode(fact["subject"])
                len_subject = len(subject_tokenized)

                facts_processed[speaker].append((
                    subject_tokenized + fact_tokenized,
                    len_subject * [self._t_fact_subject] + len_fact * [self._t_fact_body],
                    fact
                ))
                return
            elif fact["relation"] == "has_plot":
                facts_processed[speaker].append((
                    fact_tokenized,
                    len_fact * [self._t_fact_plot],
                    fact
                ))
                return
            elif fact["relation"] == "has_role":
                # TODO: has_role is movie-title + role NOT actor :-( change this!!!
                return
                subject_tokenized = self.tokenizer.encode(fact["subject"])
                len_subject = len(subject_tokenized)
                fact_tokenized_for_role = self.tokenizer.encode("plays the character {} .".format(fact["object"]))
                len_fact_tokenized_for_role = len(fact_tokenized_for_role)
                facts_processed[speaker].append((
                    subject_tokenized + fact_tokenized_for_role,
                    len_subject * self._t_fact_subject + len_fact_tokenized_for_role * self._t_fact_body,
                    fact
                ))
                stop = "here"
                # return
            elif fact["relation"] == "has_budget":
                body_token = self._t_fact_budget
            elif fact["relation"] == "has_age_certificate":
                body_token = self._t_fact_certificate
            elif fact["relation"] == "has_shot_location":
                body_token = self._t_fact_location
            elif fact["relation"] == "has_genre":
                body_token = self._t_fact_genre
            elif fact["relation"] == "has_release_year":
                body_token = self._t_fact_year
            elif fact["relation"] == "has_actor":
                body_token = self._t_fact_actor
            elif fact["relation"] == "has_writer":
                body_token = self._t_fact_writer
            elif fact["relation"] == "has_director":
                body_token = self._t_fact_director
            else:
                raise Exception("{} not implemented".format(fact["relation"]))
            facts_processed[speaker].append((
                fact_tokenized,
                len_fact * [body_token],
                fact
            ))

        if inference:
            facts_processed = {"inference": []}
            speakers = ["inference"]
            facts = {"inference": facts}
        else:
            facts_processed = {"first_speaker": [], "second_speaker": []}
            speakers = ["first_speaker", "second_speaker"]

        for speaker in speakers:
            ss_facts = facts[speaker]
            for fact in ss_facts:
                # TODO: _replace_special_tokens needed?
                if self.hps.context_paraphrased:
                    #if inference:
                    #    print("Fact : {}".format(fact))
                    #    print("Type of fact : {}".format(type(fact)))
                    #    print("Length of fact : {}".format(len(fact)))
                    #    print("Keys of fact : {}".format(fact.keys()))
                    subj = fact["subject"] if self.hps.remove_delex else fact["subject_type"]
                    obj = fact["object"] if self.hps.remove_delex else fact["object_delex"]
                    fact_sentence = create_fact_sentence(subj=subj, relation=fact["relation"], obj=obj,
                                                         separate_komma=False)
                    fact_tokenized = self.tokenizer.encode(fact_sentence)
                elif self.hps.context_triples:
                    subj = fact["subject"] if self.hps.remove_delex else fact["subject_type"]
                    obj = fact["object"] if self.hps.remove_delex else fact["object_delex"]
                    relation = fact["relation"].replace("has_", "")
                    relation = relation.replace("_", " ")
                    # We re-use the <FACT:ACTOR0> as an "end of fact" token here.
                    fact_sequence = "" + subj + " " + relation + " " + str(obj) + " <FACT:ACTOR0>"
                    fact_tokenized = self.tokenizer.encode(fact_sequence)
                else:
                    if not self.hps.remove_delex:
                        fact_tokenized = self.tokenizer.encode(fact["object_delex"])
                    else:
                        fact_tokenized = self.tokenizer.encode(str(fact["object"]))
                len_fact = len(fact_tokenized)
                if self.hps.remove_delex:
                    if self.hps.context_paraphrased or self.hps.context_triples:
                        token = "<FACT:MOVIE>"
                        create_seqs_without_delex_v2()
                    else:
                        token = Moviecorpus._get_correct_fact_token(subject=fact["subject"],
                                                                    relation=fact["relation"])
                        create_seqs_without_delex()
                else:
                    if self.hps.context_paraphrased:
                        token = "<FACT:MOVIE>"
                    else:
                        token = Moviecorpus._get_correct_fact_token(subject=fact["subject_type"],
                                                                    relation=fact["relation"])
                    create_seqs_with_delex()

        if inference:
            facts_processed = facts_processed["inference"]

        return facts_processed

    def _process_and_binarize_attitudes(self, attitudes, attitude_sentences, inference=False):
        """   """
        if inference:
            attitudes_processed = {"inference": []}
            speakers = ["inference"]
            attitudes = {"inference": attitudes}
        else:
            attitudes_processed = {"first_speaker": [], "second_speaker": []}
            speakers = ["first_speaker", "second_speaker"]

        for speaker in speakers:
            ss_atts = attitudes[speaker]
            for num, attitude in enumerate(ss_atts):
                token = Moviecorpus._get_correct_att_token(subject=attitude["subject"], relation=attitude["relation"])
                if attitude_sentences is not None:
                    repl_att_sent = Moviecorpus._replace_special_tokens([attitude["source"]])
                    ss = self.tokenizer.encode(repl_att_sent[0])
                    attitudes_processed[speaker].append((
                        ss,
                        len(ss) * self.tokenizer.encode(token)
                    ))
                else:
                    attitudes_processed[speaker].append((
                        self.tokenizer.encode("<OPRATE:{}>".format(attitude["object"])),
                        self.tokenizer.encode(token)
                    ))

        if inference:
            attitudes_processed = attitudes_processed["inference"]

        return attitudes_processed

    def _process_and_binarize_attitudes_v2(self, attitudes, attitude_sentences, inference=False):
        """ Attitude processing with new attitude encoding in the dataset.

        Not compatible with dataset version V8 or lower!
        The self.new_dataset_encoding_v9 Boolean need checked!
        """
        if inference:
            attitudes_processed = {"inference": []}
            speakers = ["inference"]
            attitudes = {"inference": attitudes}
        else:
            attitudes_processed = {"first_speaker": [], "second_speaker": []}
            speakers = ["first_speaker", "second_speaker"]

        if self.hps.remove_delex:
            source_key = "src_original"
        else:
            source_key = "src_delex"

        for speaker in speakers:
            ss_atts = attitudes[speaker]
            for num, attitude in enumerate(ss_atts):
                token = Moviecorpus._get_correct_att_token(subject=attitude["subject_type"],
                                                           relation=attitude["relation"])
                if self.hps.context_triples:
                    subj = attitude["subject"] if self.hps.remove_delex else attitude["subject_type"]
                    obj = OPINION_OBJECT_TO_TRIPLE_DICT[attitude["object"]]
                    relation = OPINION_RELATION_TO_TRIPLE_DICT[attitude["relation"]]
                    if relation == "certificate_attitude":
                        obj = random.choice(OPINION_OBJECT_TO_TRIPLE_DICT_AGE_RESTR[attitude["object"]])
                    # We re-use the <FACT:ACTOR0> as an "end of opinion" token here.
                    attitude_sequence = "" + str(subj) + " " + relation + " " + obj + " <FACT:ACTOR0>"
                    attitude_tokenized = self.tokenizer.encode(attitude_sequence)
                    attitudes_processed[speaker].append((
                        attitude_tokenized,
                        len(attitude_tokenized) * self.tokenizer.encode("<OPINION:MOVIE>"),
                        attitude
                    ))
                elif attitude_sentences is not None:
                    #repl_att_sent = Moviecorpus._replace_special_tokens(attitude[source_key])
                    repl_att_sent = Moviecorpus._replace_special_tokens([attitude[source_key]])
                    ss = self.tokenizer.encode(repl_att_sent[0])
                    attitudes_processed[speaker].append((
                        ss,
                        len(ss) * self.tokenizer.encode(token),
                        attitude
                    ))
                else:
                    attitudes_processed[speaker].append((
                        self.tokenizer.encode("<OPRATE:{}>".format(attitude["object"])),
                        self.tokenizer.encode(token),
                        attitude
                    ))

        if inference:
            attitudes_processed = attitudes_processed["inference"]

        return attitudes_processed

    def _process_subgraph(self, subgraph, inference=False):
        """ """
        def process_komodis():
            """ Uses the specific encoding for komodis, where relations are not explicitly encoded.
            kg encoding (1) in context_length_evaluation.xlsx
            """
            ss = self.tokenizer.encode(node["content"])
            subgraph_sequence[speaker].append((
                ss,
                len(ss) * [self.kg_encoding_mapping[node["type"]]],  # TODO: make sure, that is the correct token!
                node["content"],
                node["type"]
            ))

        def process_edge_node():
            """ Converts each node into a type-content sequence to explicitly encode the relations.
            kg encoding (3) in context_length_evaluation.xlsx
            """
            c_node = self.tokenizer.encode(node["content"])
            c_edge = self.tokenizer.encode(node["type"])
            subgraph_sequence[speaker].append((
                c_edge + c_node,
                len(c_edge) * [self.special_tokens_dict["<FACT:OBJECT>"]] +
                len(c_node) * [self.special_tokens_dict["<FACT:SUBJECT>"]],
                node["content"],
                node["type"]
            ))

        def process_smart():
            """ Converts each node into a type-content sequence, where the type is added on the token-type dimension.
            kg encoding (4) in context_length_evaluation.xlsx
            """
            c_node = self.tokenizer.encode(node["content"])
            c_type = self.tokenizer.encode(node["type"])
            node_length = max(len(c_node), len(c_type))

            c_node = c_node + [self.tokenizer.pad_token_id] * (node_length - len(c_node))
            c_type = c_type + [self.tokenizer.pad_token_id] * (node_length - len(c_type))

            subgraph_sequence[speaker].append((
                c_node,
                c_type,
                node["content"],
                node["type"]
            ))

        def sequence_length(seq):
            """ Returns the length of the whole sequence. """
            full_length = 0
            for item in seq:
                full_length += len(item[0])
            return full_length

        def get_single_attitudes(item_idx, matrix, sequence):
            """ """
            for num, relation in enumerate(matrix[item_idx]):
                if relation == 1 and sequence[num][3] == "attitude":
                    attitude_relations = 0
                    for att_rel in matrix[num]:
                        attitude_relations += att_rel
                    if attitude_relations <= 2:
                        return num
            return -1

        def shorten_context(sequence, matrix):
            """ """
            while sequence_length(sequence) > self.hps.max_context_length:
                remove_candidates = []
                for idx, item in enumerate(sequence):
                    if item[3] not in ["movie", "attitude"]:
                        remove_candidates.append(idx)
                remove_candidate = random.choice(remove_candidates)
                matrix = np.delete(matrix, remove_candidate, axis=0)
                matrix = np.delete(matrix, remove_candidate, axis=1)
                sequence.pop(remove_candidate)

                for idx, item in enumerate(sequence):
                    if item[3] == "attitude":
                        att_rels = 0
                        for att_rel in matrix[idx]:
                            att_rels += att_rel
                        if att_rels == 1:
                            matrix = np.delete(matrix, idx, axis=0)
                            matrix = np.delete(matrix, idx, axis=1)
                            sequence.pop(idx)
                            break

            return sequence, matrix

        if inference:
            subgraph_sequence = {"inference": []}
            subgraph_attn_mask = {"inference": []}
            speakers = ["inference"]
        else:
            subgraph_sequence = {"first_speaker": [], "second_speaker": []}
            subgraph_attn_mask = {"first_speaker": [], "second_speaker": []}
            speakers = ["first_speaker", "second_speaker"]

        for speaker in speakers:
            for node in subgraph[speaker]["nodes"]:
                if str(node["content"]) == "-1" and node["type"] == "age restriction":
                    node["content"] = "unknown"
                if self.hps.kg_encoding_type == "komodis":
                    process_komodis()
                elif self.hps.kg_encoding_type == "edge_node":
                    process_edge_node()
                elif self.hps.kg_encoding_type == "smart":
                    process_smart()
                else:
                    raise ValueError("Could not find a kg_encoding_type "
                                     "with name: {}".format(self.hps.kg_encoding_type))
            subgraph_attn_mask[speaker] = np.array(json.loads(subgraph[speaker]["matrix"]))

        # make knowledge graphs shorter, if needed
        if self.hps.max_context_length > -1:
            for speaker in speakers:
                subgraph_sequence[speaker], subgraph_attn_mask[speaker] = \
                    shorten_context(subgraph_sequence[speaker], subgraph_attn_mask[speaker])

        return {"sequence": subgraph_sequence, "attn_matrix": subgraph_attn_mask}

    @staticmethod
    def _shuffle_nodes(nodes):
        """ Shuffles nodes and returns shuffle-indices. """
        indices = list(range(len(nodes)))
        nodes_indices = list(zip(nodes, indices))
        random.shuffle(nodes_indices)
        return zip(*nodes_indices)

    @staticmethod
    def _shuffle_matrix(matrix, indices):
        """ Shuffles an attention-matrix based on shuffled indices. """
        temp_matrix_1 = np.ndarray((matrix.shape[0], matrix.shape[1]))
        temp_matrix_2 = np.ndarray((matrix.shape[0], matrix.shape[1]))

        for n, i in enumerate(indices):
            temp_matrix_1[:, n] = matrix[:, i]
        for n, i in enumerate(indices):
            temp_matrix_2[n, :] = temp_matrix_1[i, :]

        return temp_matrix_2

    @staticmethod
    def _expand_matrix(matrix, lengths, fix_length=None):
        """ Expands the attention-matrix based on the node-lengths """
        temp_matrix_1 = np.ndarray((sum(lengths), matrix.shape[0]))
        temp_matrix_2 = np.ndarray((sum(lengths), sum(lengths)))

        curr = 0
        for n, i in enumerate(lengths):
            temp_matrix_1[curr:curr+i, :] = np.repeat([matrix[n, :]], repeats=i, axis=0)
            curr += i
        curr = 0
        for n, i in enumerate(lengths):
            temp_matrix_2[:, curr:curr+i] = np.transpose(np.repeat([temp_matrix_1[:, n]], repeats=i, axis=0))
            curr += i

        if fix_length is not None:
            background_matrix = np.zeros((fix_length, fix_length), dtype=int)
            background_matrix[0:temp_matrix_2.shape[0], 0:temp_matrix_2.shape[1]] = temp_matrix_2
            temp_matrix_2 = background_matrix

        return temp_matrix_2

    def __create_attitudes_as_sentences(self, attitude, subject):
        """ Creates real sentences for attitudes instead of single value tokens.

            This function is called within _process_and_binarize_attitudes.
        """
        stop = "here"
        for mask in ["'MOVIE'", "PERSON", "COUNTRY", "BUDGET", "TYPE", "GENRE"]:
            if mask in attitude and mask != "TYPE":
                attitude = attitude.replace(mask, TOKEN_ENCODING_MAPPING[subject])
            elif mask in attitude:
                if "actor" in subject:
                    attitude = attitude.replace("TYPE", "actor")
                elif "director" in subject:
                    attitude = attitude.replace("TYPE", "director")
                elif "writer" in subject:
                    attitude = attitude.replace("TYPE", "writer")
                else:
                    raise BaseException("{} not covered by __create_attitutes_as_sentences(...)".format(subject))

        print(attitude)
        return attitude

    @staticmethod
    def _get_correct_fact_token(subject, relation):
        if relation == "has_plot":
            return "<FACT:PLOT>"
        if subject == "movie#0":
            return "<FACT:MOVIE>"
        if subject == "actor#0":
            return "<FACT:ACTOR0>"
        if subject in ["actor#1", "actor#2"]:
            return "<FACT:ACTOR1>"
        if subject == "writer#0":
            return "<FACT:WRITER>"
        if subject == "director#0":
            return "<FACT:DIRECTOR>"
        stop = "here"

    @staticmethod
    def _get_correct_att_token(subject, relation):
        if relation == "has_bot_certificate_attitude":
            return "<OPINION:CERTIFICATE>"
        if relation == "has_bot_budget_attitude":
            return "<OPINION:BUDGET>"
        if subject == "movie#0":
            return "<OPINION:MOVIE>"
        if subject == "actor#0":
            return "<OPINION:ACTOR0>"
        if subject in ["actor#1", "actor#2"]:
            return "<OPINION:ACTOR1>"
        if subject == "writer#0":
            return "<OPINION:WRITER>"
        if subject == "director#0":
            return "<OPINION:DIRECTOR>"
        if subject in ["genre#0", "genre#1"]:
            return "<OPINION:GENRE>"
        if subject == "country#0":
            return "<OPINION:COUNTRY>"
        stop = "here"

    @staticmethod
    def _replace_special_tokens(utterances):
        """ In the original dataset, the special tokens differ from the one defined here. This function replaces
         the original tokens with the ones from the tokenizer.
         """
        utterances_fixed = []
        for utterance in utterances:
            #ByMe
            if isinstance(utterance,list):
                print("Utterance: {}".format(utterance))
            for original, new in TOKEN_ENCODING_MAPPING.items():
                utterance = utterance.replace(original, new)
            utterances_fixed.append(utterance)
        return utterances_fixed

    @staticmethod
    def _replace_special_moviecorpus_tokens(dialogue):
        """ Replaces [eou] tokens and add [end] tokens.
        """
        new_dialogue = []
        for utterance in dialogue:
            tokens = utterance.split(" ")
            new_tokens = []
            for i in range(len(tokens)):
                if i == 0:
                    new_tokens.append(tokens[i])
                else:
                    if tokens[i] in ["[eou]", "[EOU]"]:
                        if tokens[i - 1] in ["?", ".", ",", "!", ";", ":"]:
                            continue
                        else:
                            new_tokens.append(".")
                    else:
                        new_tokens.append(tokens[i])
            new_dialogue.append(" ".join(new_tokens))
        return new_dialogue

    def convert_clear_txt_to_sequences(self,
                                       facts=None,
                                       attitudes=None,
                                       history=None,
                                       reply=None,
                                       kg_nodes=None,
                                       kg_attn_matrix=None):
        """ For inference only! """
        assert history is not None or reply is not None  # One of history, reply must be != None!
        encoded_kg_nodes = []
        if facts is not None:
            #ByMe
            facts = self._process_and_binarize_facts_v2(facts, inference=True)
            #facts = self._process_and_binarize_facts(facts, inference=True)
            attitudes = self._process_and_binarize_attitudes_v2(attitudes,
                                                                attitude_sentences=self.hps.attitude_sentences,
                                                                inference=True)

        elif kg_nodes is not None:
            for node in kg_nodes:
                ss = self.tokenizer.encode(node["content"])
                encoded_kg_nodes.append((
                    ss,
                    len(ss) * [self.kg_encoding_mapping[node["type"]]]
                ))
            node_lengths = [len(x[0]) for x in encoded_kg_nodes]
            kg_attn_matrix = Moviecorpus._expand_matrix(kg_attn_matrix, node_lengths)
        else:
            raise ValueError("kg information or facts and attitudes must be != None!")

        if history is not None:
            # history = [d.lower() for d in history]
            history = Moviecorpus._replace_special_tokens(history)
            history = Moviecorpus._replace_special_moviecorpus_tokens(history)
            history = [self.tokenizer.encode(d) for d in history]
        else:
            history = []

        if reply is None:
            reply = []
        elif len(reply) > 0:
            reply = self.tokenizer.encode(reply)
        seqs = self._convert_sample_to_sequences(facts, attitudes, history, reply, lm_labels=False, inference=True)
        return seqs

    @staticmethod
    def create_facts_and_attitudes(movie):
        """ Creates baseline facts and attitudes for the context of user userid. """

        # create basic facts
        facts = [{
            "subject": movie.title,
            "relation": "has_plot",
            "object": movie.get_fact("plot")},
            {"subject": movie.title,
             "relation": "has_release_year",
             "object": str(movie.get_fact("year"))},
            {"subject": movie.title,
             "relation": "has_shot_location",
             "object": movie.get_fact("countries")[0]},
            {"subject": movie.title,
             "relation": "has_genre",
             "object": movie.get_fact("genres")[0]},
            {"subject": movie.title,
             "relation": "has_age_certificate",
             "object": str(movie.get_fact("certificate"))},
            {"subject": movie.title,
             "relation": "has_budget",
             "object": str(movie.get_fact("budget"))}]
        if len(movie.get_fact("genres")) > 1:
            facts.append({"subject": movie.title,
                          "relation": "has_genre",
                          "object": movie.get_fact("genres")[1]})

        # create basic attitudes
        attitudes = [{
            "subject": movie.title,
            "subject_type": "movie#0",
            "relation": "has_general_bot_attitude",
            "object": 3,
            "source_original": "I like '{}'.".format(movie.title),
            "source_delex": "I like 'movie#0'."},
            {"subject": movie.get_fact("genres")[0],
             "subject_type": "genre#0",
             "relation": "has_general_bot_attitude",
             "object": 3,
             "source_original": "I like {} movies.".format(movie.get_fact("genres")[0]),
             "source_delex": "I like genre#0 movies."},
            {"subject": movie.title,
             "subject_type": "movie#0",
             "relation": "has_bot_certificate_attitude",
             "object": 3,
             "source_original": "I agree with the age restriction.",
             "source_delex": "I agree with the age restriction."}
        ]
        if len(movie.get_fact("genres")) > 1:
            attitudes.append({"subject": movie.get_fact("genres")[1],
                              "subject_type": "genre#0",
                              "relation": "has_general_bot_attitude",
                              "object": 3,
                              "source_original": "I like {} movies.".format(movie.get_fact("genres")[1]),
                              "source_delex": "I like genre#0 movies."})

        return facts, attitudes

    @staticmethod
    def create_person_facts_and_attitude(movie, person=None, attitude=None, person_type="actor"):
        """ Creates facts for an (un-)specific actor with an (un-)specific attitude.
        """
        def get_trivia(p):
            """ Returns None or a trivia as string. """
            # trivia
            _trivia = None
            for _key, _dataperson in movie._person_ref.items():
                if _dataperson.name == str(p):
                    if _dataperson.has_trivia("None"):
                        _trivia = _dataperson.get_trivia("None")
                        break
            return _trivia

        trivia = None

        # --- get person object, if a specific person was requested (person != None) ----------------------------------
        if person_type == "actor":
            if person is not None:
                actors = movie.get_fact("actors")
                for item in actors:
                    if str(item) == person:
                        person = item
                        break
        elif person_type == "writer":
            if person is not None:
                person = movie.get_fact("writer")
        elif person_type == "director":
            if person is not None:
                person = movie.get_fact("director")

        # --- create a trivia, if possible (if random actor, we try to get one with a trivia) -------------------------
        if isinstance(person, str) or person is None:
            if person_type == "actor":
                # always choose an actor with a trivia
                ns = [0, 1, 2, 3, 4, 5]
                random.shuffle(ns)
                for n in ns:
                    person = movie.get_fact("actors")[n]
                    trivia = get_trivia(p=person)
                    if trivia is not None:
                        break
            else:
                trivia = get_trivia(p=person)

        if attitude is not None:
            print("WARNING: Not yet implemented!")

        # --- create objects for the conversation model ---------------------------------------------------------------
        facts = [{
            "subject": movie.title,
            "relation": "has_{}".format(person_type),
            "object": str(person)
        }]

        # add trivia
        if trivia is not None:
            facts.append({
                "subject": str(person),
                "relation": "has_trivia",
                "object": trivia
            })

        attitudes = [{
            "subject": str(person),
            "subject_type": "{}#0".format(person_type),
            "relation": "has_general_bot_attitude",
            "object": 3,
            "source_original": "I like {}.".format(str(person)),
            "source_delex": "I like {}#0.".format(person_type)
        }]

        return facts, attitudes

    def _convert_sample_to_sequences(self,
                                     facts,
                                     attitudes,
                                     history,
                                     reply,
                                     lm_labels=True,
                                     inference=False):

        facts_and_attitudes = copy.deepcopy(facts + attitudes)
        random.shuffle(facts_and_attitudes)
        context_input_ids = list(chain(*[x[0] for x in facts_and_attitudes]))
        context_token_type_ids = list(chain(*[x[1] for x in facts_and_attitudes]))
        if len(context_token_type_ids) != len(context_input_ids):
            stop = "Here"
        facts_input_ids = list(chain(*[x[0] for x in facts]))
        facts_token_type_ids = list(chain(*[x[1] for x in facts]))
        atts_input_ids = list(chain(*[x[0] for x in attitudes]))
        atts_token_type_ids = list(chain(*[x[1] for x in attitudes]))
        context_input_ids = facts_input_ids + atts_input_ids
        context_token_type_ids = facts_token_type_ids + atts_token_type_ids

        # The network should always predict a systems answer. So depending on the number of previous utterances,
        # the first utterance type can be either system or other:  # TODO: IMPLEMENT THAT, CHECK IPAD NOTES ...
        hist_length = len(history)
        if hist_length % 2 == 0:
            first_utt_type = self.special_tokens_dict["<SPK:S>"]
            second_utt_type = self.special_tokens_dict["<SPK:O>"]
        else:
            first_utt_type = self.special_tokens_dict["<SPK:O>"]
            second_utt_type = self.special_tokens_dict["<SPK:S>"]

        sequence = copy.deepcopy([[self.tokenizer.bos_token_id] + context_input_ids] + history + [reply])
        if not inference:
            sequence[-1] += [self.tokenizer.eos_token_id]
        sequence = [sequence[0]] + [[second_utt_type if i % 2
                                     else first_utt_type] + s
                                    for i, s in enumerate(sequence[1:])]
        seqs = {
            "input_ids": list(chain(*sequence))
        }

        if len(seqs["input_ids"]) > 64:
            stop = "here"

        def cond(i):
            if i % 2:
                return second_utt_type
            return first_utt_type

        seqs["token_type_ids"] = [self.tokenizer.bos_token_id] + context_token_type_ids + \
                                 [cond(i) for i, s in enumerate(sequence[1:]) for _ in s]

        seqs["mc_token_ids"] = len(seqs["input_ids"]) - 1
        if lm_labels:
            seqs["lm_labels"] = ([-100] * sum(len(s) for s in sequence[:-1])) + [-100] + sequence[-1][1:]
        else:
            seqs["lm_labels"] = [-100] * len(seqs["input_ids"])

        return seqs

    def _convert_dialogue_to_samples_light(self,
                                     dialogue,
                                     split,
                                     history_length=(1, 15),
                                     max_length=32):
        """ Converts one dialogue in all possible samples, given some settings.

        history_length  (a, b) Number of history utterances between a and b, if possible.

        """

        def get_context_length(fs=None, atts=None):
            len_facts = len(list(chain(*[x[0] for x in fs])))
            len_atts = len(list(chain(*[x[0] for x in atts])))
            len_context = len_facts + len_atts
            return len_context

        samples = []
        for num in range(len(dialogue["dialogue_binarized"]) - 1):
            # determine which speaker is system for current sample
            if num % 2 == 0:
                speaker = "second_speaker"
            else:
                speaker = "first_speaker"

            # number of previous utterances
            r = np.random.randint(history_length[0], history_length[1] + 1)
            lower = num + 1 - r
            if lower < 0:
                lower = 0

            # check and analyse max length of context
            if self.hps.kg_encoding:
                facts_src = None
                attitudes_src = None
            else:
                facts_src = copy.deepcopy(dialogue["facts_binarized"][speaker])
                attitudes_src = copy.deepcopy(dialogue["attitudes_binarized"][speaker])
            if self.hps.max_context_length > -1:
                len_context = get_context_length(facts_src, attitudes_src)
                while len_context > self.hps.max_context_length:
                    if len(attitudes_src) == 0 and len(facts_src) > 0:
                        random_item_from_list = random.choice(facts_src)
                        facts_src.remove(random_item_from_list)
                    elif len(attitudes_src) > 0 and len(facts_src) == 0:
                        random_item_from_list = random.choice(attitudes_src)
                        attitudes_src.remove(random_item_from_list)
                    elif random.random() < 0.5:
                        random_item_from_list = random.choice(facts_src)
                        facts_src.remove(random_item_from_list)
                    else:
                        random_item_from_list = random.choice(attitudes_src)
                        attitudes_src.remove(random_item_from_list)
                    len_context = get_context_length(facts_src, attitudes_src)

            # check for max length
            t = 0
            skip = False
            while True:
                len_hist = len(list(chain(*dialogue["dialogue_binarized"][lower + t:num + 1])))
                len_label = len(dialogue["dialogue_binarized"][num + 1])

                len_context = get_context_length(facts_src, attitudes_src)

                # 3 tokens:              start token, end token, token for reply
                # (num + 1 - lower -t):  plus one token per utterance in the history
                num_special_tokens = 3 + (num + 1 - lower - t)

                # check both length: correct utterance and the longest wrong utterance
                if (len_hist + len_context + num_special_tokens) <= max_length and \
                        (len_hist + len_label + len_context + num_special_tokens) <= max_length:
                    break

                t += 1

                if lower + t == num + 1:
                    skip = True
                    break

            # --- knowledge graph encoding ---
            # Shuffles the knowledge graph sequence and matrix and then expand the matrix to an attention matrix.
            facts = None
            attitudes = None
            if self.hps.kg_encoding:
                nodes_shuffled, indices = Moviecorpus._shuffle_nodes(dialogue["kg_encoded"]["sequence"][speaker])
                node_lengths = [len(x[0]) for x in nodes_shuffled]
                matrix_shuffled = Moviecorpus._shuffle_matrix(dialogue["kg_encoded"]["attn_matrix"][speaker], indices)
            else:
                facts = facts_src
                attitudes = attitudes_src

            if not skip:
                samples.append({
                    "label_utterance": [dialogue["dialogue_binarized"][num + 1]],
                    "dialogue_history": dialogue["dialogue_binarized"][lower + t:num + 1],
                    "facts": facts,
                    "attitudes": attitudes
                })

        return samples

    def _convert_dialogues_to_samples(self, split, num_wrong_utts=1):
        samples = []
        print("[Dialogues_to_samples] Processing {}-data.".format(split))
        for dialogue in tqdm(self.dataset[split]):
            if self.debug and ("dialogue_binarized" not in dialogue or len(samples) > 256):
                break
            samples += self._convert_dialogue_to_samples_light(dialogue, split, (5, 5),
                                                         self.hps.max_input_length)
        return samples

    def _compute_wrong_dialogues(self, split, num, only_movie=None, exclude_id=None):
        """ Returns random wrong utterances

        Args:
            split       A string. One of: "train", "valid", "test".
            num         An integer. Number of wrong utterances.
            only_movie  A string. If given, only utterances of the given movie are returned.
            exclude_id  An integer. If given, the movie with that prepared_id is ignored.

        """
        if only_movie is not None:
            candidates = [movie for movie in self.dataset[split] if movie['movie_title'] == only_movie]
        else:
            candidates = np.random.choice(self.dataset[split], 10)

        utterances = []
        for movie in candidates:
            if exclude_id is not None:
                if movie["story"][2]['prepared_id'] == exclude_id:
                    continue
            # --- if not binarized (can happen in debug mode), binarize: ---
            if "dialogue_binarized" not in movie:
                if self.hps.remove_delex:
                    movie["dialogue_processed"] = [d.lower() for d in movie["dialogue_original"]]

                else:
                    movie["dialogue_processed"] = Moviecorpus._replace_special_tokens(movie["dialogue_ner_full"])
                movie["dialogue_processed"] = \
                    Moviecorpus._replace_special_moviecorpus_tokens(movie["dialogue_processed"])
                movie["dialogue_binarized"] = [self.tokenizer.encode(d) for d in movie["dialogue_processed"]]
            # --------------------------------------------------------------
            utterances += movie['dialogue_binarized']
        np.random.shuffle(utterances)

        if len(utterances) < num:
            pool = copy.deepcopy(utterances)
            np.random.shuffle(pool)
        while len(utterances) < num:
            diff = num - len(utterances)
            if diff < len(utterances):
                utterances += pool[:diff]
            else:
                utterances += pool
                np.random.shuffle(pool)

        return utterances[:num]

    @staticmethod
    def _choose_difficult_wrong_utterances(dialogue, num_wrong_utts, same_movie_cands):
        """ In a first shot, only num_wrong_utts == 3 or num_wrong_utts == 7 is alloweed.
        This simplifies the algorithm to choose wrong utterances.

        """
        assert 3 <= num_wrong_utts <= 9  # Out of that boundaries, the creation may fail!

        wrong_utts = []
        for wrong_utts_source in dialogue["wrong_candidates_processed"]:
            subset_wrong_utts = []
            # always add wrong opinions if available
            if len(wrong_utts_source["wrong_opinions"]) > 0:
                subset_wrong_utts.append(Moviecorpus._random_choice(wrong_utts_source["wrong_opinions"]))

            # always add similar repeats if available
            if len(wrong_utts_source["similar_repeats"]) > 0:
                subset_wrong_utts.append(Moviecorpus._random_choice(wrong_utts_source["similar_repeats"]))

            # add similar to context if max 1 wrong candidate found so far
            if len(subset_wrong_utts) < 2 and len(wrong_utts_source["similar_to_context"]) > 0:
                subset_wrong_utts.append(Moviecorpus._random_choice(wrong_utts_source["similar_to_context"]))

            # if nothing found so far, add a similar one
            if len(subset_wrong_utts) == 0:
                subset_wrong_utts.append(wrong_utts_source["repeats"][0])

            # fill with random utterances
            idx = 0
            while len(subset_wrong_utts) < num_wrong_utts and idx < 5:
                if same_movie_cands:
                    subset_wrong_utts.append(wrong_utts_source["random_same_title"][idx])
                else:
                    subset_wrong_utts.append(wrong_utts_source["random"][idx])
                idx += 1

            np.random.shuffle(subset_wrong_utts)
            wrong_utts += subset_wrong_utts
            # subset_wrong_utts = []
            # if len(wrong_utts_source["similar_repeats"]) > 1:
            #     subset_wrong_utts.append(Moviecorpus._random_choice(wrong_utts_source["similar_repeats"]))
            # elif np.random.rand() < 0.33:
            #     subset_wrong_utts.append(wrong_utts_source["repeats"][0])
            # if len(wrong_utts_source["wrong_opinions"]) > 1:
            #     subset_wrong_utts.append(Moviecorpus._random_choice(wrong_utts_source["wrong_opinions"]))
            # if len(wrong_utts_source["similar_to_context"]) > 1:
            #     subset_wrong_utts.append(Moviecorpus._random_choice(wrong_utts_source["similar_to_context"]))
            # incr_random = 0
            # incr_ramdom_same_movie = 0
            # while len(subset_wrong_utts) < num_wrong_utts:
            #     if incr_random < 5 and (np.random.rand() < 0.5 or incr_ramdom_same_movie >= 5):
            #         subset_wrong_utts.append(wrong_utts_source["random"][incr_random])
            #         incr_random += 1
            #     else:
            #         subset_wrong_utts.append(wrong_utts_source["random_same_title"][incr_ramdom_same_movie])
            #         incr_ramdom_same_movie += 1
            # np.random.shuffle(subset_wrong_utts)
            # wrong_utts += subset_wrong_utts
            # stop = "here"

        return wrong_utts

    @staticmethod
    def _split_binarized_corpus(dataset):
        """ Splits the corpus in train, eval and test. """

        # collect movie titles
        titles = []
        for dialogue in dataset:
            # compute movie title
            if dialogue['story'][2]['story_type'] == "PersonToMovieStory":
                dialogue["movie_title"] = dialogue['story'][0]['entities'][2]
            else:
                dialogue["movie_title"] = dialogue['story'][0]['entities'][0]
            if dialogue['movie_title'] not in titles:
                titles.append(dialogue['movie_title'])
        num_train = int(len(titles) * 0.8)
        num_eval = int(len(titles) * 0.1)
        np.random.shuffle(titles)
        titles_split = {
            "train": titles[0:num_train],
            "valid": titles[num_train:num_train + num_eval],
            "test": titles[num_train + num_eval:]
        }
        dataset_splitted = {
            "train": [],
            "valid": [],
            "test": []
        }
        for dialogue in dataset:
            if dialogue['movie_title'] in titles_split['train']:
                dataset_splitted["train"].append(dialogue)
            elif dialogue['movie_title'] in titles_split['valid']:
                dataset_splitted["valid"].append(dialogue)
            elif dialogue['movie_title'] in titles_split['test']:
                dataset_splitted["test"].append(dialogue)

        return dataset_splitted

    def _pad_features(self, features, padding):
        for name in ["input_ids", "token_type_ids", "lm_labels"]:
            aux_seq= [len(f) for f in features[name]]
            if not aux_seq:
                print("Sequence empty for "+name)
            else:
                print("Max length of {} is: {}".format(name, max(len(f) for f in features[name])))
        aux_seq= [len(f) for f in features["input_ids"]]
        if not aux_seq:
            print("Sequence empty for input_ids")
            max_l = 0
        else:
            max_l = max(len(feature) for feature in features["input_ids"])
        if self.debug:
            # If debug mode, we want to see if the maximum size of input data fits into the vram for training.
            max_l = self.hps.max_input_length
        print("Padding to {}".format(max_l))
        for name in ["input_ids", "token_type_ids", "lm_labels"]:
            features[name] = [x + [padding if name != "lm_labels" else -100] * (max_l - len(x)) for x in features[name]]

        if self.hps.kg_encoding:
            if not [m.shape[0] for m in features["kg_attn_matrix"]]:
                max_l = 0
            else:
                max_l = max(m.shape[0] for m in features["kg_attn_matrix"])
            print("Padding to {}".format(max_l))
            for num, matrix in enumerate(features["kg_attn_matrix"]):
                back = np.tril(max_l * [1])
                d1, d2 = matrix.shape
                back[:d1, :d2] = matrix
                features["kg_attn_matrix"][num] = back
                stop = "here"
            stop = "here"
        return features

    def convert_graph_to_triples(self, subgraph):
        """ Converts a subgraph into triples.

        Given a subgraph, this function returns a list of triple-items, that can be used to train a model
        with the default fact and opinion encoding.
        """
        raise NotImplementedError()
        def process_fact():
            """ """
            if item["predicate"] == "is-a":
                new_relation = FACT_RELATION_GRAPH_TRIPLE_DICT[item["object"]]
                # TODO: graph_depth == 1 HACK: Subject ist immer derselbe title
                subject = movie_zero
                correct_subject = get_movie_title(item)
                object = item["subject"]
                # relations = get_relations(item_idx=idx)
                # subject = get_movie_title(item["subject"])
            else:
                new_relation = FACT_RELATION_GRAPH_TRIPLE_DICT[item["predicate"]]
                subject = item["subject"]
                object = item["object"]

            facts.append({
                "subject": subject,
                "subject_type": None,
                "relation": new_relation,
                "object": object,
                "object_delex": None
            })
            stop = "here"

        def is_processed():
            """ Checks, whether a triple is already processed as a fact or not. """
            for fact in facts:
                if item["subject"] == fact["object"]:
                    return True
            return False

        def process_opinion():
            """ """
            inverse_opinion_obj_dict = {v: k for k, v in OPINION_OBJECT_TO_TRIPLE_DICT.items()}
            if item["object"] in inverse_opinion_obj_dict:
                new_object = inverse_opinion_obj_dict[item["object"]]
                new_relation = "has_general_bot_attitude"
            else:
                new_object = IVERSE_OBJECT_TO_TRIPLE_AGE_RESTR[item["object"]]
                new_relation = "has_bot_certificate_attitude"
            new_subject = item["subject"]

            # estimate subject_type
            subject_type = None
            for triple in subgraph["triples"]:
                if triple["subject"] == item["subject"] and triple["predicate"] == "is-a":
                    subject_type = TRIPLE_OPINION_SUBJECT_TYPE_DICT[triple["object"]]
                    break
            if subject_type is None:  # hack
                subject_type = "genre#0"

            opinion = {
                "subject": new_subject,
                "subject_type": subject_type,
                "relation": new_relation,
                "object": new_object,
                "src_original": "",
                "src_delex": None
            }
            #src_original, _ = compute_opinion_sentence(attitude=opinion)
            #opinion["src_original"] = src_original
            opinions.append(opinion)

        def get_relations(item_idx):
            """ """
            rs = []
            for num in range(matrix.shape[0]):
                relation = matrix[item_idx][num]
                if relation == 1:
                    rs.append(subgraph["nodes"][num])
            return rs

        def get_movie_title(node):
            for idx, item in enumerate(subgraph["nodes"]):
                if item["content"] == node["subject"] and item["type"] == node["object"]:
                    relations = get_relations(item_idx=idx)
            stop = "here"
            return ""

        facts = []
        opinions = []

        movie_id_dict = {}
        movie_zero = ""
        matrix = np.array(json.loads(subgraph["matrix"]))
        for idx, item in enumerate(subgraph["nodes"]):
            if item["type"] == "movie":
                movie_id_dict[item["content"]] = idx
                movie_zero = item["content"]

        for idx, item in enumerate(subgraph["triples"]):
            if item["predicate"] in ["attitude", "random_attitude"]:
                process_opinion()
            elif item["predicate"] in FACT_RELATION_GRAPH_TRIPLE_DICT:
                process_fact()

        return facts, opinions

    def create_context_statistics(self, split="train"):
        """ Creates statistics about the context. """
        assert self.dataset is not None

        per_dialogue_statistics = []
        for dialogue in self.dataset[split]:
            if "facts_binarized" in dialogue:
                for speaker in ["first_speaker", "second_speaker"]:
                    len_facts = int(np.sum([len(x[0]) for x in dialogue["facts_binarized"][speaker]]))
                    len_atts = int(np.sum([len(x[0]) for x in dialogue["attitudes_binarized"][speaker]]))

                    statistic_item = {
                        "len_facts": len_facts,
                        "len_atts": len_atts,
                        "len_context": len_atts + len_facts
                    }
                    per_dialogue_statistics.append(statistic_item)
            elif "kg_encoded" in dialogue:
                for speaker in ["first_speaker", "second_speaker"]:
                    len_context = int(np.sum([len(x[0]) for x in dialogue["kg_encoded"]["sequence"][speaker]]))
                    statistic_item = {
                        "len_facts": 0,
                        "len_atts": 0,
                        "len_context": len_context
                    }
                    per_dialogue_statistics.append(statistic_item)

        # histogram
        histogram = {
            "0-64": 0,
            "65-128": 0,
            "129-192": 0,
            "193-256": 0,
            ">256": 0
        }
        for item in per_dialogue_statistics:
            if item["len_context"] <= 64:
                histogram["0-64"] += 1
            elif item["len_context"] <= 128:
                histogram["65-128"] += 1
            elif item["len_context"] <= 192:
                histogram["129-192"] += 1
            elif item["len_context"] <= 256:
                histogram["193-256"] += 1
            else:
                histogram[">256"] += 1
        # average
        sum_facts = 0
        sum_atts = 0
        sum_context = 0
        for item in per_dialogue_statistics:
            sum_facts += item["len_facts"]
            sum_atts += item["len_atts"]
            sum_context += item["len_context"]
        averages = {
            "facts": sum_facts / len(per_dialogue_statistics),
            "atts": sum_atts / len(per_dialogue_statistics),
            "context": sum_context / len(per_dialogue_statistics)
        }
        print("Histogram:")
        for key, value in histogram.items():
            print("{}: {}".format(key, value))
        print("Averages:")
        for key, value in averages.items():
            print("{}:{}".format(key, value))

    @staticmethod
    def _merge_attitudes_for_remove_delex(original, processed, no_speaker=False):
        """ This function creates a new set of attitudes where the source is not delexicalised.
        Used only for training!
        """
        merged_attitudes = copy.deepcopy(processed)

        if no_speaker:
            for attitude, original_attitude in zip(merged_attitudes, original):
                attitude["source"] = original_attitude["source"]
        else:
            for speaker in ["first_speaker", "second_speaker"]:
                for attitude, original_attitude in zip(merged_attitudes[speaker], original[speaker]):
                    attitude["source"] = original_attitude["source"]

        return merged_attitudes

    @staticmethod
    def _random_choice(input_list):
        """ Returns one random element from a list of lists. """
        num = len(input_list)
        rnum = np.random.randint(0, num)
        return input_list[rnum]


if __name__ == "__main__":
    config_parser = ConfigParser()
    curr_dir = os.path.dirname(os.path.realpath(__file__))
    config_parser.read(os.path.join(curr_dir, "config.ini"))

    obj = Moviecorpus(
        path_to_data=config_parser.get("datasets", "path_komodis"),
        tokenizer= GPT2Tokenizer.from_pretrained("gpt2"), #transformers.OpenAIGPTTokenizer.from_pretrained("openai-gpt"),
        debug = 'True' == config_parser.get("run", "debug")
    )
    obj.load_txt_dataset(train=True, valid=True, test=True)
    obj.tokenize_dataset()
    # obj.create_context_statistics()

    sampler, loader = obj.get_torch_features(split="train", batch_size=32)
    print(type(sampler))
    print(type(loader))
    print("Size of Loader: " + str(len(loader.dataset)))

    stop = "here"