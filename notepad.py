SPECIAL_TOKENS = ["<SST>", "<END>", "<PAD>", "<SPK:S>", "<SPK:O>", "<DEL:MOVIE>", "<DEL:ACTOR>", "<DEL:DIRECTOR>",
                  "<DEL:WRITER>", "<DEL:YEAR>", "<DEL:BUDGET>", "<DEL:CERTIFICATE>", "<DEL:COUNTRY>", "<DEL:GENRE0>",
                  "<DEL:GENRE1>", "<FACT:MOVIE>", "<FACT:ACTOR0>", "<FACT:ACTOR1>", "<FACT:DIRECTOR>", "<FACT:WRITER>",
                  "<FACT:PLOT>", "<FACT:SUBJECT>", "<FACT:OBJECT>", "<OPINION:MOVIE>", "<OPINION:ACTOR>",
                  "<OPINION:DIRECTOR>", "<OPINION:WRITER>", "<OPINION:COUNTRY>", "<OPINION:GENRE>", "<OPINION:BUDGET>",
                  "<OPINION:CERTIFICATE>", "<OPRATE:0>", "<OPRATE:1>", "<OPRATE:2>", "<OPRATE:3>", "<OPRATE:4>",
                  "<OPRATE:5>"]