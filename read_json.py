import json
from transformers import cached_path
from transformers import GPT2LMHeadModel, GPT2Tokenizer

with open('valid_dialogues_mini.json') as f:
  data = json.load(f)

#print("Len of file: {}".format(len(data)))
indiana = data[0]
print(json.dumps(indiana, indent = 4, sort_keys=True))

#------------------------------------------------------
#dataset_path = 'valid_dialogues.json'
#personachat_file = cached_path(dataset_path)
#print(personachat_file)
#with open(personachat_file, "r", encoding="utf-8") as f:
#    dataset = json.loads(f.read())
#print(json.dumps(dataset[0], indent = 4, sort_keys=True))

#tipo = type(dataset)
#print(tipo)

#tokenizer_class, model_class = (GPT2Tokenizer, GPT2LMHeadModel)
#tokenizer = tokenizer_class.from_pretrained("gpt2")

#def tokenize(obj):
#    if isinstance(obj, int):
#        obj = str(obj)
#    if isinstance(obj, str):
#        return tokenizer.convert_tokens_to_ids(tokenizer.tokenize(obj))
#    if isinstance(obj, dict):
#        return dict((n, tokenize(o)) for n, o in obj.items())
#    return list(tokenize(o) for o in obj)


#dataset = tokenize(dataset)
#print(type(dataset))