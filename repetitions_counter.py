import os

def repetitions_count():
    files = os.listdir('interactions')
    save_path = "interactions"
    for file_name in files:
        save_file = os.path.join(save_path, file_name)
        with open(save_file,'r') as f:
            Lines = f.readlines()
            n = 0
            repetition_count = 0
            for line in Lines:
                n += 1
                line = line.lower()
                words = line.split()
                for i in range(0, len(words)):
                    count = 1
                    for j in range(i + 1, len(words)):
                        if words[i] == (words[j]):
                            repetition_count += 1
            repetition_rate = repetition_count *100 / n
            print("Repetition rate in file {} : {}".format(file_name,repetition_rate))

if __name__ == '__main__':
    repetitions_count()