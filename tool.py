import PySimpleGUI as sg
from labeler import Labeler

labeler = Labeler()
example, _ = labeler.get_example()
f1,f2,a1,a2 = labeler.get_facts_and_attributes(example)
dialogue = labeler.get_dialogue(example)

first_column = [
    [sg.Text("[Facts 1]")],
    [sg.Text("{} {}: {}".format(it['subject'], it['relation'], it['object'])) for it in f1],
    [sg.Text("[Facts 2]")],
    [sg.Text("{} {}: {}".format(it['subject'], it['relation'], it['object'])) for it in f2]
]

second_column = [
    [sg.Text("Dialogue")],
    [sg.Text(str(i) + ": " + element) for i, element in enumerate(dialogue)]
]

layout = [
    [
        sg.Column(first_column,size=(600,200), expand_x = False),
        sg.VSeperator(),
        sg.Column(second_column),
        sg.Button('Ok')
    ]
]

# Create the window
window = sg.Window("Labeler", layout, size=(750,350))

# Create an event loop
while True:
    event, values = window.read()
    # End program if user closes window or
    # presses the OK button
    if event == "OK" or event == sg.WIN_CLOSED:
        break

window.close()
