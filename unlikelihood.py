import torch
from torch import Tensor
import torch.nn.functional as F
import torch.nn
from torch.nn.modules.loss import _WeightedLoss
from typing import Optional
from interact import top_filtering

class NGramIterator:
    """
    N-Gram iterator for a list.
    """
    def __init__(self, lst, n):
        self.lst = lst
        self.n = n
        self.max = len(lst) - n

    def __iter__(self):
        self.counter = -1
        return self

    def __next__(self):
        self.counter += 1
        if self.counter > self.max:
            raise StopIteration
        return tuple(self.lst[self.counter : self.counter + self.n])

class OwnLoss(_WeightedLoss):
    __constants__ = ['ignore_index', 'reduction']
    ignore_index: int

    def __init__(self, weight: Optional[Tensor] = None, size_average=None, ignore_index: int = -100,
                 reduce=None, reduction: str = 'mean') -> None:
        super(OwnLoss, self).__init__(weight, size_average, reduce, reduction)
        self.ignore_index = ignore_index
        self.alpha = 0.3

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        assert self.weight is None or isinstance(self.weight, Tensor)
        like_loss = F.cross_entropy(input, target, weight=self.weight,
                               ignore_index=self.ignore_index, reduction=self.reduction)

        unlike_loss = torch.rand(like_loss.size())
        #unlike_loss = self.compute_un_loss(batch)

        loss = like_loss + self.alpha*unlike_loss

        return loss

    def compute_un_loss(self, input: Tensor, target: Tensor, return_output=False):

        # Generate
        clamp_min = 1e-6 if self.opt['fp16'] else 1e-20
        with torch.no_grad():
            top_pred_scores = top_filtering(input)
            #beam_pred_scores, _ = self._generate(batch, self.beam_size, maxlen)

        logits, preds = top_pred_scores, input

        # construct mask marking repeats
        n = self.opt['seq_ul_n']  # label n-grams
        crep_mask = torch.zeros_like(pred_toks).type_as(logits)
        lrep_mask = torch.zeros_like(pred_toks).type_as(logits)

        for i, gen in enumerate(generations):
            gen_i = gen.tolist()

            # Collect context ngrams
            context_i = batch.text_vec[i].tolist()
            context_n_grams = self._count_n_grams(context_i, n)

            seen_n_grams = defaultdict(int)

            # penalize if there is a context repeat
            for j, n_gram in enumerate(NGramIterator(gen_i, n)):
                if context_n_grams[n_gram] > 0:
                    crep_mask[i, j : j + n] = 1

            # penalize if there is a label repeat
            for j, n_gram in enumerate(NGramIterator(gen_i, n)):
                if seen_n_grams[n_gram] > 0:
                    lrep_mask[i, j : j + n] = 1
                seen_n_grams[n_gram] += 1

        # Compute unlikelihood loss
        lprobs = self.pred_logsoftmax(logits)
        pred_lprobs = lprobs.view(-1, lprobs.size(2)).gather(1, pred_toks.view(-1, 1))
        one_minus_probs = torch.clamp((1.0 - pred_lprobs.exp()), min=clamp_min).view(
            pred_toks.size(0), pred_toks.size(1)
        )

        mask = ((1 - self.opt['ctxt_beta']) * lrep_mask) + (
            self.opt['ctxt_beta'] * crep_mask
        )

        ul_loss = -(torch.log(one_minus_probs)) * mask
        total_loss = div(ul_loss.sum(), mask.sum())
        self.record_local_metric(
            'ul_loss', AverageMetric.many(ul_loss.sum(dim=-1), mask.sum(dim=-1))
        )

        if not self.is_training:
            # in eval mode, we want metrics (e.g. PPL) provided by tga's compute_loss
            _, _ = super().compute_loss(batch, return_output=True)

        if return_output:
            return total_loss, model_output
        return total_loss

class unlikelihood():
    def __init__(self):
        parser = ArgumentParser()
        parser.add_argument("--file", type=str, default="valid", help="Json file to classify: train, valid or test")
        args = parser.parse_args()
        self.file_prefix = args.file
        self.dataset = self.get_dataset()
        self.vocabulary = self.get_vocabulary()
        self.example = self.get_random_example()

    def get_dataset(self):
        original_json_path = '{}_dialogues.json'.format(self.file_prefix)
        with open(original_json_path, "r", encoding="utf-8") as f:
            dataset = json.loads(f.read())
        return dataset

    def get_random_example(self):
        N = len(self.dataset)
        j = np.random.randint(0, N)
        example = self.dataset[j]
        return example

    def display_example(self):
        print("Example : {}".format(self.example))

    def display_dialogue(self):
        dialogue = self.example['dialogue_original']
        for element in dialogue:
            print(element)

    def tokens_dialoge(self):
        dialogue = self.example['dialogue_original']
        T = 0
        for element in dialogue:
            for token in element:
                T+=1
        return T

    def get_vocabulary(self):
        vocabulary = []
        for instance in self.dataset:
            dialogue = instance['dialogue_original']
            for element in dialogue:
                tokens = element.split()
                for t in tokens:
                    if t not in vocabulary:
                        vocabulary.append(t)
        return vocabulary

    def get_distribution_vocabulary(self):
        words = {}
        for instance in self.dataset:
            dialogue = instance['dialogue_original']
            for element in dialogue:
                tokens = element.split()
                for t in tokens:
                    if t in words:
                        words[t] += 1
                    else:
                        words[t] = 1
        S = 0
        for k,v in words.items():
            S += v
        p = {k: c/S for k, c in words.items()}
        return p

    def get_C_repeat(self):
        dialogue = self.example['dialogue_original']
        words = {}
        for element in dialogue:
            tokens = element.split()
            for t in tokens:
                if t in words:
                    words[t] += 1
                else:
                    words[t] = 1
        #words = {k: v for k, v in words.items() if not k in stopwords.words()}
        repeated_words = {k: v for k, v in words.items() if v > 1}
        return repeated_words

    def get_C_in_t_repeat(self,t):
        dialogue = self.example['dialogue_original']
        words = {}
        count = 0
        reached = False
        for element in dialogue:
            tokens = element.split()
            for tok in tokens:
                count += 1
                if count == t:
                    reached = True
                    break
                if tok in words:
                    words[tok] += 1
                else:
                    words[tok] = 1
            if reached:
                break
        #words = {k: v for k, v in words.items() if not k in stopwords.words()}
        repeated_words = {k: v for k, v in words.items() if v > 1}
        return repeated_words

    def get_C_in_t_identity(self,t):
        dialogue = self.example['dialogue_original']
        words = {}
        count = 0
        reached = False
        for element in dialogue:
            tokens = element.split()
            for t in tokens:
                count += 1
                if count == t:
                    reached = True
                    break
                if t in words:
                    words[t] += 1
                else:
                    words[t] = 1
            if reached:
                break
        return words

    def unlike_repeat(self,p,y):
        suma = 0
        T = self.tokens_dialoge()
        for t in range(T):
            C = self.get_C_in_t_repeat(t)
            for k,v in C.items():
                if k == y:
                    suma += 1-np.log(1-p[y])
        return -suma

    def unlike_vocabulary(self,p,y):
        suma = 0
        T = self.tokens_dialoge()
        for t in range(T):
            C = self.get_C_in_t_identity(t)
            S = 0
            for k, v in C.items():
                S += v
            p_model = {k: c / S for k, c in C.items()}
            for k, v in C.items():
                if y in p_model:
                    beta = p[y] * np.log(p_model[y]/p[y])
                    suma += beta*(1 - np.log(1 - p[y]))
        return -suma

    def compute_loss(self,batch):
        scores, preds, *_ = model_output  # scores is bsz x time x vocab

        scores = F.log_softmax(scores, dim=-1)
        scores_view = scores.view(-1, scores.size(-1))
        targets = batch.label_vec
        targets_view = targets.view(-1)
        notnull = targets.ne(self.NULL_IDX)
        ul_notnull = notnull & (batch.rewards < 0).unsqueeze(1).expand_as(notnull)
        ul_target_tokens = ul_notnull.long().sum()
        range_ = torch.arange(targets_view.size(0)).to(batch.label_vec.device)
        ul_scores = scores_view[range_, targets_view]
        clamp_min = 1e-6 if self.opt['fp16'] else 1e-20
        ul_loss = (
                -torch.log(torch.clamp(1.0 - ul_scores.exp(), min=clamp_min)).view_as(
                    ul_notnull
                )
                * ul_notnull.float()
        ).sum()
        self.global_metrics.add('ul_loss', AverageMetric(ul_loss, ul_target_tokens))
        if ul_target_tokens > 0:
            ul_loss /= ul_target_tokens

if __name__ == "__main__":
    unlike = unlikelihood()
    p_words = unlike.get_distribution_vocabulary()
    print("Words in dataset: {}".format(len(p_words)))
    suma = 0
    for k,v in p_words.items():
        suma += v
    print("Sum of probabilities: {}".format(suma))
    dialogue_vocabulary = unlike.get_C_repeat().keys()
    for y in dialogue_vocabulary:
        L_repeat = unlike.unlike_repeat(p_words,y)
        if L_repeat != 0:
            print("Repetition Unlikelihood of {}: {}".format(y, L_repeat))
        L_vocab = unlike.unlike_vocabulary(p_words,y)
        print("Vocabulary Unlikelihood of {}: {}".format(y, L_vocab))




